<?php

$page_title = "Home";

?>

<?php include('inc/header.php'); ?>

<section class="main-slider">

	<div class="rev_slider_wrapper fullwidthbanner-container" id="rev_slider_one_wrapper" data-source="gallery">
		<div class="rev_slider fullwidthabanner" id="rev_slider_one" data-version="5.4.1">
			<ul>

				<li data-transition="parallaxvertical" data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1688" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/1.jpg">
					<img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="images/main-slider/1.jpg">

					<div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-width="['650','650','650','450']" data-whitespace="normal" data-hoffset="['15','15','15','15']" data-voffset="['-10','-10','-80','-80']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"delay":0,"speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
						<div class="border-layer"></div>
					</div>

					<div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-width="['650','650','650','450']" data-whitespace="normal" data-hoffset="['80','80','15','15']" data-voffset="['-60','-60','-80','-80']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
						<h2>Providing a 360 degree solution<br></h2>
					</div>

					<div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-width="['650','650','650','450']" data-whitespace="normal" data-hoffset="['80','80','15','15']" data-voffset="['55','55','30','30']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"delay":1500,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
						<div class="text">which is specific to each customer</div>
					</div>

					<div class="tp-caption tp-resizeme" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-whitespace="normal" data-width="['650','650','650','450']" data-hoffset="['270','270','15','15']" data-voffset="['155','155','120','120']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"delay":2000,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
						<div class="link-box">
							<a href="/about.php" class="theme-btn btn-style-two">Learn more</a>
						</div>
					</div>

					<div class="tp-caption tp-resizeme" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-whitespace="normal" data-width="['650','650','650','450']" data-hoffset="['480','480','15','15']" data-voffset="['155','155','120','120']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"delay":2500,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
						<div class="icons-box clearfix">
							<span class="icon flaticon-manufacturing"></span>
							<span class="icon flaticon-robot-1"></span>
							<span class="icon flaticon-helmet-2"></span>
						</div>
					</div>

				</li>

				<li data-transition="parallaxvertical" data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1689" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/2.jpg" data-title="Slide Title">
					<img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="images/main-slider/2.jpg">

					<div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-width="['650','650','650','450']" data-whitespace="normal" data-hoffset="['15','15','15','15']" data-voffset="['-10','-10','-80','-80']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"delay":0,"speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
						<div class="border-layer"></div>
					</div>

					<div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-width="['650','650','650','450']" data-whitespace="normal" data-hoffset="['80','80','15','15']" data-voffset="['-60','-60','-80','-80']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
						<h2>Highly-trained & committed team </h2>
					</div>

					<div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-width="['650','650','650','450']" data-whitespace="normal" data-hoffset="['80','80','15','15']" data-voffset="['55','55','30','30']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"delay":1500,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
						<div class="text">dedicated to deliver high quality work</div>
					</div>

					<div class="tp-caption tp-resizeme" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-whitespace="normal" data-width="['650','650','650','450']" data-hoffset="['270','270','15','15']" data-voffset="['155','155','120','120']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"delay":2000,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
						<div class="link-box">
							<a href="/about.php" class="theme-btn btn-style-two">Learn more</a>
						</div>
					</div>

					<div class="tp-caption tp-resizeme" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-whitespace="normal" data-width="['650','650','650','450']" data-hoffset="['480','480','15','15']" data-voffset="['155','155','120','120']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"delay":2500,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
						<div class="icons-box clearfix">
							<span class="icon flaticon-manufacturing"></span>
							<span class="icon flaticon-robot-1"></span>
							<span class="icon flaticon-helmet-2"></span>
						</div>
					</div>

				</li>
				<li data-transition="parallaxvertical" data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1690" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/3.jpg" data-title="Slide Title">
					<img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="images/main-slider/3.jpg">

					<div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-width="['650','650','650','450']" data-whitespace="normal" data-hoffset="['15','15','15','15']" data-voffset="['-10','-10','-80','-80']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"delay":0,"speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
						<div class="border-layer"></div>
					</div>

					<div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-width="['650','650','650','450']" data-whitespace="normal" data-hoffset="['80','80','15','15']" data-voffset="['-60','-60','-80','-80']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
						<h2> Integration between diverse techniques</h2>
					</div>

					<div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-width="['650','650','650','450']" data-whitespace="normal" data-hoffset="['80','80','15','15']" data-voffset="['55','55','30','30']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"delay":1500,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
						<div class="text">in mechanical, electrical and plumbing services</div>
					</div>

					<div class="tp-caption tp-resizeme" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-whitespace="normal" data-width="['650','650','650','450']" data-hoffset="['270','270','15','15']" data-voffset="['155','155','120','120']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"delay":2000,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
						<div class="link-box">
							<a href="/about.php" class="theme-btn btn-style-two">Learn more</a>
						</div>
					</div>

					<div class="tp-caption tp-resizeme" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-whitespace="normal" data-width="['650','650','650','450']" data-hoffset="['480','480','15','15']" data-voffset="['155','155','120','120']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"delay":2500,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
						<div class="icons-box clearfix">
							<span class="icon flaticon-manufacturing"></span>
							<span class="icon flaticon-robot-1"></span>
							<span class="icon flaticon-helmet-2"></span>
						</div>
					</div>

				</li>

			</ul>
		</div>
	</div>
</section>
<!--End Main Slider-->

<!-- Innovation Section -->
<section class="innovation-section">
	<div class="auto-container">
		<div class="row clearfix">


			<div class="content-column col-lg-6 col-md-12 col-sm-12">
				<div class="inner-column">
					<!-- Sec Title -->
					<div class="sec-title wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="title">Airtech Nepal</div>
						<h2>Reliability Matters<br></h2>
					</div>
					<div class="bold-text">Airtech has established itself as the largest and the most preferred HVAC Company in Nepal for two decades now.</div>
					<div class="text">Airtech provides a high level support to its customer base, which ranges from small capacity room AC to huge commercial chillers. We have expertise in supporting systems from globally renowned manufacturers and we consistently train our staff to stay up to date on all new products and services in our industry.</div>

					<!-- Counter Box -->
					<div class="counter-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">

						<div class="fact-counter">
							<div class="clearfix">

								<!--Column-->
								<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
									<div class="inner">
										<div class="count-outer count-box">
											<span class="count-text" data-speed="2000" data-stop="20">0</span>+
											<h4 class="counter-title">Years Experience</h4>
										</div>
									</div>
								</div>

								<!--Column-->
								<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
									<div class="inner">
										<div class="count-outer count-box">
											<span class="count-text" data-speed="2500" data-stop="36">0</span>
											<h4 class="counter-title">Number of MEPs Projects</h4>
										</div>
									</div>
								</div>

								<!--Column-->
								<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
									<div class="inner">
										<div class="count-outer count-box">
											<span class="count-text" data-speed="3000" data-stop="300">0</span>+
											<h4 class="counter-title">Dedicated Team Members</h4>
										</div>
									</div>
								</div>

							</div>

						</div>

					</div>

				</div>
			</div>

			<!-- Imaages Column -->
			<div class="images-column col-lg-6 col-md-12 col-sm-12">
				<div class="inner-column">
					<div class="image wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
						<img src="images/resource/innovator.png" alt="" />
					</div>
					<div class="image wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
						<img src="images/resource/innovator-1.1.jpg" alt="" />
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
<!-- End Innovation Section -->

<!-- Services Section -->
<section class="services-section">
	<div class="auto-container">
		<div class="row clearfix">

			<!-- Services Block -->
			<div class="services-block col-xl-3 col-lg-6 col-md-6 col-sm-12">
				<div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
					<div class="big-icon flaticon-settings-4"></div>
					<div class="icon-box">
						<span class="icon flaticon-drill-3"></span>
					</div>
					<h3><a class='no-click' href='#'>Quality Work</a></h3>
					<div class="text">Delivery of high quality work within the stipulated time frame.
					</div>
					<a class="arrow no-click" href="#"><span class="icon flaticon-next"></span></a>
				</div>
			</div>

			<!-- Services Block -->
			<div class="services-block col-xl-3 col-lg-6 col-md-6 col-sm-12">
				<div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
					<div class="big-icon flaticon-settings-4"></div>
					<div class="icon-box">
						<span class="icon flaticon-safety"></span>
					</div>
					<h3><a href="#" class='no-click'>Industrial Specialist</a></h3>
					<div class="text">Pioneer of MEP design and installation in Nepal - providing a single stop for our customers..</div>
					<a class="arrow no-click" href="#"><span class="icon flaticon-next"></span></a>
				</div>
			</div>

			<!-- Services Block -->
			<div class="services-block col-xl-3 col-lg-6 col-md-6 col-sm-12">
				<div class="inner-box wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
					<div class="big-icon flaticon-settings-4"></div>
					<div class="icon-box">
						<span class="icon flaticon-tools-1"></span>
					</div>
					<h3><a href="#" class='no-click'>Latest and Sustainable Technology</a></h3>
					<div class="text">use of cutting edge technology to focus on energy efficient and environment friendly solutions
					</div>
					<a class="arrow no-click" href="#"><span class="icon flaticon-next"></span></a>
				</div>
			</div>

			<!-- Services Block -->
			<div class="services-block col-xl-3 col-lg-6 col-md-6 col-sm-12">
				<div class="inner-box wow fadeInUp" data-wow-delay="900ms" data-wow-duration="1500ms">
					<div class="big-icon flaticon-settings-4"></div>
					<div class="icon-box">
						<span class="icon flaticon-tape-measure"></span>
					</div>
					<h3><a href="#" class='no-click'>Competent and collaborative Team</a></h3>
					<div class="text">An extremely dedicated and qualified team of Project Managers, Engineers and Expert Technicians etc.
					</div>
					<a class="arrow no-click" href="#"><span class="icon flaticon-next"></span></a>
				</div>
			</div>

		</div>
	</div>
</section>
<!-- End Services Section -->

<!-- Fluid Section One -->
<section class="fluid-section-one">
	<div class="outer-container clearfix">

		<!--Image Column-->
		<div class="image-column wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms" style="background-image:url(images/resource/innovator.png);">
			<figure class="image-box"><img src="images/resource/image-1.1.jpg" alt=""></figure>
		</div>

		<!--Content Column-->
		<div class="content-column">
			<div class="inner-column">
				<!-- Sec Title -->
				<div class="sec-title">
					<h2>About Airtech</h2>
				</div>
				<div class="text">Established in the year 2000, Airtech offers the best engineering solutions for a wide spectrum of customers in areas related to Heating, Ventilation & Air Conditioning (HVAC). It is the largest and the most preferred air conditioning company in Nepal. We provide a 360 degree solution because we focus on excellence and personalized service, to provide the highest quality professional installation of (HVAC) Heating, Ventilation and Air Conditioning systems. </div>
				<ul class="list-style-one">
					<li>Expert Solution</li>
					<li>Incredible Service Support</li>
					<li>Unmatched Engineering and Design</li>
					<li>Committed to maximise energy efficiency</li>
				</ul>
				<!-- Signature Box -->

			</div>
		</div>
	</div>
</section>
<!-- End Fluid Section One -->

<!-- Services Section Two -->
<section class="services-section-two">
	<div class="auto-container">
		<div class="inner-container">
			<div class="big-icon flaticon-settings-4"></div>
			<!-- Sec Title -->
			<div class="sec-title light wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
				<div class="row clearfix">
					<div class="pull-left col-xl-4 col-lg-5 col-md-12 col-sm-12">
						<h2><a href='/services.php'>Services</a> <br></h2>
					</div>
					<div class="pull-left col-xl-8 col-lg-7 col-md-12 col-sm-12">
						<div class="text">Airtech MEP projects are one stop solution for the construction fundamentals into systematic,
							comprehensive and collaborated solution for effective operation.</div>
					</div>
				</div>
			</div>

			<div class="three-item-carousel owl-carousel owl-theme">

				<!-- Services Block Two -->
				<div class="services-block-two">
					<div class="inner-box">
						<div class="image">
							<a href="/services/hvac.php"><img src="images/resource/hvac.jpg" alt="" /></a>
						</div>
						<div class="lower-content">
							<h3><a href="/services/hvac.php">HVAC</a></h3>
							<div class="text">Airtech understands the characteristics and demands of air-conditioning in different fields. Each area has its distinct requirement and we advise the client with the system based on their precise requirement.</div>

						</div>
					</div>
				</div>

				<!-- Services Block Two -->
				<div class="services-block-two">
					<div class="inner-box">
						<div class="image">
							<a href="/services/electrical-services.php"><img src="images/resource/electrical-services.jpg" alt="" /></a>
						</div>
						<div class="lower-content">
							<h3><a href="/services/electrical-services.php">Electrical Services</a></h3>
							<div class="text">Airtech has an expert team of highly competent
								and experienced electrical engineer. We are able to
								understand specific requirement of various institutional
								clients like hotels, resorts, hospitals, malls, industries
								etc.</div>

						</div>
					</div>
				</div>

				<!-- Services Block Two -->
				<div class="services-block-two">
					<div class="inner-box">
						<div class="image">
							<a href="/services/plumbing-services.php"><img src="images/resource/plumbing.jpg" alt="" /></a>
						</div>
						<div class="lower-content">
							<h3><a href="/services/plumbing-services.php">Plumbing Services</a></h3>
							<div class="text">Our plumbing engineers having expertise in Execution
								of Plumbing jobs. We have a “whole building” approach
								and are instrumental in influencing high water efficiency,
								energy, fire protection, pollution systems and a
								sustainable site.</div>

						</div>
					</div>
				</div>

				<!-- Services Block Two -->
				<div class="services-block-two">
					<div class="inner-box">
						<div class="image">
							<a href="/services/firefighting-systems.php"><img src="images/resource/firefighting.jpg" alt="" /></a>
						</div>
						<div class="lower-content">
							<h3><a href="/services/firefighting-systems.php">Firefighting Systems</a></h3>
							<div class="text">Our expertise is in the Execution of special fire hazard
								technology for fire protection systems.
								This service protects people and critical assets from
								dangers of fire.</div>

						</div>
					</div>
				</div>

				<!-- Services Block Two -->
				<div class="services-block-two">
					<div class="inner-box">
						<div class="image">
							<a href="/services/service-support.php"><img src="images/resource/service-support.jpg" alt="" /></a>
						</div>
						<div class="lower-content">
							<h3><a href="/services/service-support.php">Service Support</a></h3>
							<div class="text">
								Airtech provides a one year warranty after the successful execution of a project. If any clients wish for longer support, they have a choice to get a longer duration of support under Annual Maintenance Cost (AMC)
							</div>

						</div>
					</div>
				</div>


			</div>
		</div>
	</div>
</section>
<!-- End Services Section Two -->

<!-- Quote Section -->

<!-- End Quote Section -->

<!-- Projects Section -->
<section class="projects-section">
	<div class="auto-container">
		<!-- Sec Title -->
		<div class="sec-title centered">
			<h2>Ongoing Projects</h2>
		</div>
		<!-- Projects Carousel -->
		<div class="projects-carousel">
			<div class="image-column">
				<div class="carousel-outer">

					<div class="image-carousel owl-carousel owl-theme">

						<!-- Slide Item -->
						<div class="slide-item">
							<div class="row clearfix">

								<div class="image-column col-lg-6 col-md-12 col-sm-12">
									<div class="inner-column">
										<div class="image">
											<img src="images/gallery/1.jpg" alt="" />
										</div>
									</div>
								</div>

								<div class="content-column col-lg-6 col-md-12 col-sm-12">
									<div class="inner-column">
										<h3>Mustang Mountain Home, Jomsom</h3>
										<div class="text">A one of it’s kind resort built in the village in the foothills of Annapurna. Airtech has finished a full MEP project in high altitudes.</div>
										<a href="/projects/hotels-and-resorts.php" class="read-more">view detail <span class="arrow fas fa-angle-right"></span></a>

									</div>
								</div>
							</div>
						</div>

						<!-- Slide Item -->
						<div class="slide-item">
							<div class="row clearfix">

								<div class="image-column col-lg-6 col-md-12 col-sm-12">
									<div class="inner-column">
										<div class="image">
											<img src="images/gallery/1.jpg" alt="" />
										</div>
									</div>
								</div>

								<div class="content-column col-lg-6 col-md-12 col-sm-12">
									<div class="inner-column">
										<h3>Tiger Palace Resort, Bhairahawa</h3>
										<div class="text">
											Tiger Palace Resort is a luxurious accommodation in Bhairahawa. With a full fledged casino this is a huge tourist attraction. This was the first time Airtech did a spa and swimming pool, along with the remaining MEP services that we provide, for a resort. </div>
										<a href="/projects/hotels-and-resorts.php" class="read-more">view detail <span class="arrow fas fa-angle-right"></span></a>

									</div>
								</div>
							</div>
						</div>
						<!--
						<div class="slide-item">
							<div class="row clearfix">
								
								<div class="image-column col-lg-6 col-md-12 col-sm-12">
									<div class="inner-column">
										<div class="image">
											<img src="images/gallery/1.jpg" alt="" />
										</div>
									</div>
								</div>
								
								<div class="content-column col-lg-6 col-md-12 col-sm-12">
									<div class="inner-column">
										<h3>Ncell Corporate Office, Lainchour</h3>
										<div class="text">
										NCELL is the first private mobile operator in Nepal. Their 10 story corporate office is an on-going project where we are providing all our MEP services.
											</div>
										<a href="/projects/telecom.php" class="read-more">view detail <span class="arrow fas fa-angle-right"></span></a>

									</div>
								</div>
							</div>
						</div>

						<div class="slide-item">
							<div class="row clearfix">
								
								<div class="image-column col-lg-6 col-md-12 col-sm-12">
									<div class="inner-column">
										<div class="image">
											<img src="images/gallery/1.jpg" alt="" />
										</div>
									</div>
								</div>
								
								<div class="content-column col-lg-6 col-md-12 col-sm-12">
									<div class="inner-column">
										<h3>Banks and Corporate offices</h3>
										<div class="text">Airtech is proudly associated with Nepal’s leading commercial banks, financial institutions
											and corporate offices. We understand their specific requirement to provide the ultimate
											solution in terms of optimum performance, highest energy efficiency, maintaining aesthetics
											looks as per architect & interior designer.</div>
										<a href="/projects/banks-and-corporate-institutions.php" class="read-more">view detail <span class="arrow fas fa-angle-right"></span></a>

									</div>
								</div>
							</div>
						</div>

						<div class="slide-item">
							<div class="row clearfix">
								
								<div class="image-column col-lg-6 col-md-12 col-sm-12">
									<div class="inner-column">
										<div class="image">
											<img src="images/gallery/1.jpg" alt="" />
										</div>
									</div>
								</div>
								
								<div class="content-column col-lg-6 col-md-12 col-sm-12">
									<div class="inner-column">
										<h3>Auditoriums, Theaters & Studios</h3>
										<div class="text">Airtech understands the specific requirement of different types of auditoriums halls,
											theatres , studios, recording studio, FM studio, lecture halls etc. Our system design
											includes the following major criteria.</div>
										<a href="/projects/auditorium-halls-theaters-studios.php" class="read-more">view detail <span class="arrow fas fa-angle-right"></span></a>

									</div>
								</div>
							</div>
						</div>

						<div class="slide-item">
							<div class="row clearfix">
								
								<div class="image-column col-lg-6 col-md-12 col-sm-12">
									<div class="inner-column">
										<div class="image">
											<img src="images/gallery/1.jpg" alt="" />
										</div>
									</div>
								</div>
								
								<div class="content-column col-lg-6 col-md-12 col-sm-12">
									<div class="inner-column">
										<h3>Telecom</h3>
										<div class="text">Telecom companies have a very unique requirement of Air Conditioning. They need Air Conditioning
											for their equipments to perform at desired level round the year. Airtech values this unique requirement
											and had been able to provide energy efficient, cost effective, eco friendly and sustainable solution to
											the leading telecom operators and data centers in Nepal. Unlike comfort Air Conditioning telecom
											equipments need cooling round the year and they have high sensible heat load. Accordingly the HVAC
											equipments selected must have higher sensible load capacity ensuring that the HVAC units meet the
											requirement of highly sensitive telecom equipments.</div>
										<a href="/projects/telecom.php" class="read-more">view detail <span class="arrow fas fa-angle-right"></span></a>

									</div>
								</div>
							</div>
						</div>

						<div class="slide-item">
							<div class="row clearfix">
								
								<div class="image-column col-lg-6 col-md-12 col-sm-12">
									<div class="inner-column">
										<div class="image">
											<img src="images/gallery/1.jpg" alt="" />
										</div>
									</div>
								</div>
								
								<div class="content-column col-lg-6 col-md-12 col-sm-12">
									<div class="inner-column">
										<h3>Industries</h3>
										<div class="text">Heating, Ventilation & Air Conditioning requirement varies in different industries as per their specific
											requirement. It can be a simple comfort air conditioning for a manager room to an industrial process chilling
											for sensitive requirements. Air conditioning is required to cool a sensitive panel board which dissipates heat.
											Sometimes in industries even a fresh air and an exhaust system is installed to ensure air movement in ware
											housing to ensure that there is no microbial growth. However in some ware house there is a requirement of
											Air Conditioning if the products properties degrade in the adverse climate.</div>
										<a href="/projects/industries.php" class="read-more">view detail <span class="arrow fas fa-angle-right"></span></a>

									</div>
								</div>
							</div>
						</div>

						<div class="slide-item">
							<div class="row clearfix">
								
								<div class="image-column col-lg-6 col-md-12 col-sm-12">
									<div class="inner-column">
										<div class="image">
											<img src="images/gallery/1.jpg" alt="" />
										</div>
									</div>
								</div>
								
								<div class="content-column col-lg-6 col-md-12 col-sm-12">
									<div class="inner-column">
										<h3>Embassy and INGOs</h3>
										<div class="text">Airtech is proudly associated for the Heating,
											Ventilation and AC Requirements for many
											Embassies, INGO’s and foreign offices in Nepal. </div>
										<a href="/projects/embassies-ingo.php" class="read-more">view detail <span class="arrow fas fa-angle-right"></span></a>

									</div>
								</div>
							</div>
						</div>

-->


					</div>

					<ul class="thumbs-carousel owl-carousel owl-theme">
						<li><img src="images/gallery/thumb-1.jpg" alt=""></li>
						<li><img src="images/gallery/thumb-2.jpg" alt=""></li>
						<li><img src="images/gallery/thumb-3.jpg" alt=""></li>
						<li><img src="images/gallery/thumb-4.jpg" alt=""></li>
						<li><img src="images/gallery/thumb-5.jpg" alt=""></li>
						<li><img src="images/gallery/thumb-1.jpg" alt=""></li>
						<li><img src="images/gallery/thumb-2.jpg" alt=""></li>
						<li><img src="images/gallery/thumb-3.jpg" alt=""></li>
					</ul>

				</div>
			</div>
		</div>

	</div>
</section>
<!-- End Projects Section -->



<?php include('inc/partials/testimonials.php'); ?>


<!-- Factory Section -->
<section class="factory-section" style="background-image:url(images/background/2.png)">
	<div class="auto-container">
		<!-- Title Box -->
		<div class="title-box">
			<h2>We are very proud to be associated with</h2>
			<div class="text">We provide one-stop comprehensive MEP services to our valued customers. Our goal is to meet the needs of our customers by providing a complete, innovative and customized solution that consistently exceeds expectations.
			</div>
		</div>
		<?php include('inc/partials/clients.php'); ?>
	</div>
</section>
<!-- End Factory Section -->
<?php include('inc/footer.php'); ?>