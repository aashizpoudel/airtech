<?php include('inc/header.php'); ?>
<!--Page Title-->
<section class="page-banner style-two" style="background-image:url(images/background/services-title-bg.jpg);">
    <div class="auto-container">
        <div class="inner-container clearfix">
            <h1>Our Services</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="index-2.html">Home</a></li>
                <li>What We Do</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->

<!-- Services Section Three -->
<section class="services-section-three style-two">
    <div class="auto-container">
        <!-- Sec Title -->
        <div class="sec-title">
            <div class="row">
                <div class="col-lg-4 col-md-12 wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="title">We are Airtech</div>
                    <h2>Reliability Matters</h2>
                </div>
                <div class="col-lg-8 col-md-12 wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="text" style='margin-top:10px;margin-bottom:50px'>Airtech strives to meet the needs of our customers by providing complete turnkey engineering solution for all kinds of HVAC requirements.</div>
                </div>
            </div>

            <div class="row mt-4">

                <!-- Services Block Two -->
                <div class="services-block-three col-xl-4 col-md-6">
                    <div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="/services/hvac.php"><img src="images/resource/hvac.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="/services/hvac.php">HVAC</a></h3>
                            <div class="text my-4">
                                Airtech understands the characteristics and demands of air-conditioning in different fields. Each area has its distinct requirement and we advise the client with the system based on their precise requirement.
                            </div>

                        </div>
                    </div>
                </div>

                <!-- Services Block Two -->
                <div class="services-block-three col-xl-4 col-md-6">
                    <div class="inner-box wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="/services/electrical-services.php"><img src="images/resource/electrical-services.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="/services/electrical-services.php">Electrical Services</a></h3>
                            <div class="text my-4">Airtech has an expert team of highly competent
                                and experienced electrical engineer. We are able to
                                understand specific requirement of various institutional
                                clients like hotels, resorts, hospitals, malls, industries
                                etc.</div>

                        </div>
                    </div>
                </div>

                <!-- Services Block Two -->
                <div class="services-block-three col-xl-4 col-md-6">
                    <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="/services/plumbing-services.php"><img src="images/resource/plumbing.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="/services/plumbing-services.php">Plumbing Services</a></h3>
                            <div class="text my-4">Our plumbing engineers having expertise in Execution
                                of Plumbing jobs. We have a “whole building” approach
                                and are instrumental in influencing high water efficiency,
                                energy, fire protection, pollution systems and a
                                sustainable site.</div>

                        </div>
                    </div>
                </div>

                <!-- Services Block Two -->
                <div class="services-block-three col-xl-4 col-md-6">
                    <div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="/services/firefighting-systems.php"><img src="images/resource/firefighting.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="/services/firefighting-systems.php">Firefighting System</a></h3>
                            <div class="text my-4">Our expertise is in the Execution of special fire hazard
                                technology for fire protection systems.
                                This service protects people and critical assets from
                                dangers of fire.</div>

                        </div>
                    </div>
                </div>

                <!-- Services Block Two -->
                <div class="services-block-three col-xl-4 col-md-6">
                    <div class="inner-box wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="/services/service-support.php"><img src="images/resource/service-support.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="/services/service-support.php">Service Support</a></h3>
                            <div class="text my-4">
                                Airtech provides a one year warranty after the successful execution of a project. If any clients wish for longer support, they have a choice to get a longer duration of support under Annual Maintenance Cost (AMC). </div>

                        </div>
                    </div>
                </div>



            </div>
        </div>
</section>
<!-- End Services Section Three -->

<!-- Skill Section -->

<!-- End Skill Section -->

<?php include('inc/partials/testimonials.php'); ?>
<!-- Sponsors Section -->
<section class="sponsors-section alternate-2">
    <div class="auto-container">
        <?php include('inc/partials/clients.php'); ?>
    </div>
</section>
<!-- End Sponsors Section -->

<?php include('inc/footer.php'); ?>