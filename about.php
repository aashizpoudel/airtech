<?php
$page_title = "About Us";
include('inc/header.php'); ?>
<!--Page Title-->
<section class="page-banner style-two" style="background-image:url(images/background/about-page-title-bg.jpg);">
    <div class="auto-container">
        <div class="inner-container clearfix">
            <h1>About Airtech</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="index-2.html">Home</a></li>
                <li>About Airtech</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->

<!-- Fun Facts Section -->
<section class="about-section-two" style="background-image:url(images/background/about-section-two-bg.png);">
    <div class="auto-container">
        <div class="title-style-one style-two centered">
            <div class="icon"><img src="images/logo.png" style='width:190px' alt=""></div>
            <div class="text">

Established in the year 2000, Airtech offers the best engineering solutions for a wide spectrum of customers in areas related to Heating, Ventilation & Air Conditioning (HVAC). It is the largest and the most preferred air conditioning company in Nepal. We focus on excellence and personalized service, to provide the highest quality professional installation of (HVAC) Heating, Ventilation and Air Conditioning systems.             </div>
        </div>

        <div class="row">
            <div class="image-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <figure class="image"><a href="images/resource/about-img-1.png" class="lightbox-image"><img src="/images/resource/innovator.png" alt=""></a></figure>
                </div>
            </div>

            <div class="content-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="text" style='margin-bottom:30px'>
                    	Airtech has expanded its services to cater COMPLETE SOLUTION in (MEP) Mechanical, Electrical & Plumbing solutions. Airtech MEP commenced its services in 2013 and within a short span we have been awarded many prestigious MEP  projects. 
<br><br>
Airtech offers complete MEP contracting solutions from Procurement, Installation, Testing and Commissioning of the MEP systems that include:

                    </div>
                   <div class='row'>
                   	<div class='col-md-6'>
                   		 <ul class="list-style-one">
					<li>Internal electrification works</li>
					<li>External electrification works</li>
					<li>Plumbing works</li>
					<li>Sanitary works</li>
										<li>Sewage treatment plan</li>
					<li>Fire alarm system</li>
					<li>Hot water system</li>


				</ul>
                   	</div>
                   	<div class='col-md-6'>
                   		 <ul class="list-style-one">
							<li>Fire fighting systems</li>
					<li>Hydro Pneumatic Systems</li>
					<li>Water treatment plant</li>
					<li>Air conditioning chiller/VRF</li>
										<li>Ventilation works</li>
					<li>Access control</li>
					<li>CCTV</li>

				</ul>
                   	</div>
                   </div>
                
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Fun Facts Section -->

<!-- Missin Section -->
<section class="mission-section" style="background-image: url(images/background/mission-bg.jpg);">
    <div class="auto-container">
        <div class="row no-gutters">
            <div class="colum col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="content-box" style="background-image: url(images/icons/logo-icon-2.png);">
                        <h4>Our Mission</h4>
                        <div class="text">Airtech strives to meet the need of its customer by providing complete
                            engineered, innovative and customized technology solutions that
                            constantly exceed expectations. The mission of Airtech is to build our
                            reputation for integrity, excellence, reliability, flexibility, responsiveness,
                            innovative services and team work.</div>
                    </div>
                    <div class="image-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image"><a href="images/resource/mission-1.jpg" class="lightbox-image"><img src="images/resource/mission-1.jpg" alt=""></a></figure>
                    </div>
                </div>
                 
            </div>

            <div class="colum right-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="image-box wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image"><a href="images/resource/vision.png" class="lightbox-image"><img src="images/resource/vision.png" alt=""></a></figure>
                    </div>
                    <div class="content-box wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms" style="background-image: url(images/icons/logo-icon-2.png);">
                        <h4>Our Vision</h4>
                        <div class="text">Airtech’s success is driven by its complete focus on customers. Airtech
                            corporate vision is “Customer for Life”. We earn our customer loyalty
                            by listening to them, anticipating their requirements and working to
                            create value for them. Growth, longevity and financial success will
                            follow naturally.
                        </div>
                    </div>
                 
                </div>
            </div>
            
          
        </div>
    </div>
</section>
<!--End Missin Section -->

<!-- Quote Section -->

<!-- End Quote Section -->

<!-- FAQ Section -->
<section class="faq-section">
    <div class="auto-container">
        <div class="row">
            <!-- Accordion Column -->
            <div class="accordion-column col-lg-7 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="title-style-one">
                        <div class="icon"><img src="images/icons/logo-icon.png" alt=""></div>
                        <div class="subtitle">We are Airtech</div>
                        <h2>Reliability Matters</h2>
                    </div>

                    <!--Accordian Box-->
                    <ul class="accordion-box">
                        <!--Block-->
                        <li class="accordion block wow fadeInUp">
                            <div class="acc-btn">
                                <div class="icon-outer"><span class="icon icon-plus fa fa-angle-right"></span> </div>
                                Our Drivers
                            </div>
                            <div class="acc-content">
                                <div class="content">
                                    <div class="text">
                                    <ul class='list-style-one'>	<li>Result Oriented Approach - through an efficient and “can do attitude”.</li>
<li>Integrity - we take responsibility for what we promise</li>
<li>Excellence - achieved through proper and systematic thinking, planning and implementation.</li>
<li>Flexibility - understanding the client’s requirements.</li>
<li>Responsiveness - a receptive approach to the customer’s needs.</li>
<li>Innovative Services - research to provide creative solutions to problems </li>
<li>Teamwork - an open exchange of information and resources within our team and with our clients.</li>
</ul>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <!--Block-->
                        <li class="accordion block active-block wow fadeInUp">
                            <div class="acc-btn active">
                                <div class="icon-outer"><span class="icon icon-plus fa fa-angle-right"></span> </div>
                               Our Manpower
                            </div>
                            <div class="acc-content current">
                                <div class="content">
                                    <div class="text">Airtech has a very competent team of Project Managers, Sr. Engineers, Engineers and Technical personnel who have the experience and expertise to successfully complete the projects within a time frame. We encourage our engineers to get involved in various comprehensive training, allowing us to stay on the cutting edge of new technology. We have a strong team of over 300+ personnel.  </div>
                                </div>
                            </div>
                        </li>

                        <!--Block-->
                        <li class="accordion block wow fadeInUp">
                            <div class="acc-btn">
                                <div class="icon-outer"><span class="icon icon-plus fa fa-angle-right"></span> </div>
After Sales Service                            </div>
                            <div class="acc-content">
                                <div class="content">
                                    <div class="text">
                                    We have a dedicated team exceeding over 50 trained technical persons for the after sales services to the Customer in minimum response time.

<br>
Airtech is proud to introduce the first mobile service van to support and provide on site maintenance and troubleshooting. This would go a long way in enhancing our commitment to our valued customers for prompt and reliable service.

                                    	</div>
                                </div>
                            </div>
                        </li>

                        <!--Block-->
                       
                    </ul>
                </div>
            </div>

            <!-- image Column -->
            <div class="image-column col-lg-5 col-md-12 col-sm-12">
                <div class="inner-column">
                    <figure class="image wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms"><img src="images/resource/man-img-1.png" alt=""></figure>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End FAQ Section -->

<section class="approach-section" style="background-image: url(images/background/about-section-two-bg.png); ">
        <div class="auto-container">
            <div class="row">
                <!-- Content Column -->
                <div class="content-column col-lg-6 col-md-12 col-sm-12 order-2">
                    <div class="inner-column wow fadeInRight animated" data-wow-delay="0ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInRight;">
                        <!-- Sec Title -->
                        <div class="sec-title">
                            <h2>Message From MD</h2>
                        <div class="text">
                    
                    Airtech has been established as the largest and the most preferred HVAC Company in Nepal for around a decade.
                    Our aim has always been to meet and exceed the expectation of our customers in terms of quality products, quality services, and timely completion of the project.
Our customers appreciate the fact of having one umbrella solution for their MEP (Mechanical, Electrical, Plumbing) requirement for the better coordination and timely completion of the projects. Within a short span of time,
<br><br>
Airtech had been able to secure many highly prestigious MEP projects
The real credit of our continuous success rests on the performance of our people which also leads us to transform our company slogan as “Reliability Matters”. We are privileged to be surrounded by competent, well-motivated, highly skilled personnel with strong engineering skills. We are constantly improving our work methodology to ensure world-class services to all our clients.
<br>
We are confident that Airtech would continue to provide the best services and would help in the infrastructural development of our country. I would also like to convey my sincere thanks to all our customers, business associates, and project partners, etc. for their continuous support.
                        
                        </div>

                      
                    </div>
                </div>
                </div>

                <!-- Image Column -->
                <div class="image-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-column wow fadeInLeft animated" data-wow-delay="0ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInLeft;">
                        <figure class="image"><a href="images/resource/approach.jpg" class="lightbox-image"><img src="images/resource/Photo of MD.jpg" alt=""></a></figure>
                    </div>
                </div>

            </div>
        </div>
    </section>
<!-- Featured Services -->
<section class="featured-services">
    <div class="row no-gutters">
        <!-- Feature Block Five -->
        <div class="feature-block-five col-lg-4 col-md-6 col-sm-12 wow fadeInUp">
            <div class="inner-box" style="background-image: url(images/resource/featured-bg-1.jpg);">
                <div class="content">
                    <div class="icon-box"><span class="icon flaticon-engineer-2"></span></div>
                    <h4><a href="oil-gas.html">Expert Solution</a></h4>
                    <div class="text">Airtech has the expertise to understand the special requirements of each customer.</div>
                    
                </div>
            </div>
        </div>

        <!-- Feature Block Five -->
        <div class="feature-block-five col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="400ms">
            <div class="inner-box" style="background-image: url(images/resource/featured-bg-2.jpg);">
                <div class="content">
                    <div class="icon-box"><span class="icon flaticon-earth-globe"></span></div>
                    <h4><a href="plants.html">Service Support</a></h4>
                    <div class="text">
                    	
                    	We have a dedicated team exceeding over 50 trained technical persons for the after sales services to the Customer in minimum response time.

                    	</div>
                    
                </div>
            </div>
        </div>

        <!-- Feature Block Five -->
        <div class="feature-block-five col-lg-4 col-md-12 col-sm-12 wow fadeInUp" data-wow-delay="800ms">
            <div class="inner-box" style="background-image: url(images/resource/featured-bg-3.jpg);">
                <div class="content">
                    <div class="icon-box"><span class="icon flaticon-flask"></span></div>
                    <h4><a href="chemical-research.html">Unmatched Engineering</a></h4>
                    <div class="text">Airtech offers our customers best engineering unmatched in the construction industries.
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Featured Services -->

<!-- Team Section -->

<!--End Team Section -->
<?php include('inc/footer.php'); ?>