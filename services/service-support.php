<?php 
$page_title="Service Support";
include('./../inc/header.php');
include('./../inc/partials/services-side-nav.php');
?>
<section class="page-banner" style="background-image:url(/images/background/3.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>Service Support</h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
					<li><a href="/services.php">Services</a></li>
                    <li>Service Support</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
	
	<!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
				<!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
                	<aside class="sidebar padding-right">
						
						<!-- Category Widget -->
                        <div class="sidebar-widget categories-two">
                            <div class="widget-content">
                                <!-- Services Category -->
                                <?php echo side_nav('service-support');?>
                            </div>
                        </div>
						
						<!-- Brochures Widget -->
                        <div class="sidebar-widget brochures">
                            <div class="sidebar-title"><h5>Downloads</h5></div>
                            <div class="widget-content">
                                <a href="#" class="brochure-btn"><span class="icon fas fa-file-alt"></span> Brochure DOC</a>
                            </div>
						</div>
						
						<!-- Services Widget -->
                       				<?php include('../inc/partials/widgets.php');?>

						
					</aside>
				</div>
				
				<!--Content Side / Services Detail-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                	<div class="services-detail">
						<div class="inner-box">
							<div class="image">
								<img src="/images/resource/service-support.jpg" alt="" />
							</div>
							<div class="lower-content">
								<!-- Title Box -->
								<div class="title-box">
									<div class="title">We are Airtech</div>
									<h2>Service Support</h2>
								</div>
								<div class="bold-text">
									
									Airtech has an excellent team of competent and trained technical experts to provide prompt and efficient after sales services to its valued customers. Our engineers are trained at various manufacturing and training centers of globally renowned Air conditioning companies in Germany, Japan, Italy, Ma laysia, China , India etc.  
								
</div>
								<div class="text">
The world class training has given our technical team deep insights into the designs, selection and installation needs of each major customer application.
		<br><br><br>
		Airtech provides a one year warranty after the successful execution of a project. If any clients wish for longer support, they have a choice to get a longer duration of support under Annual Maintenance Cost (AMC) .
		
			</div>
														


								<h5>Our services at a glance ...</h5>
								<!-- Fact Counter -->
								<div class="fact-counter">
									<div class="clearfix">
										
										<!--Column-->
										<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
											<div class="inner wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
												<div class="count-outer count-box">
													<span class="count-text" data-speed="2000" data-stop="25">0</span>+
													<h4 class="counter-title">Years Experience</h4>
												</div>
											</div>
										</div>
										
										<!--Column-->
										<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
											<div class="inner wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
												<div class="count-outer count-box">
													<span class="count-text" data-speed="2500" data-stop="36">0</span>
													<h4 class="counter-title">Industries Served</h4>
												</div>
											</div>
										</div>
										
										<!--Column-->
										<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
											<div class="inner wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
												<div class="count-outer count-box">
													<span class="count-text" data-speed="3000" data-stop="105">0</span>
													<h4 class="counter-title">Factories Built</h4>
												</div>
											</div>
										</div>
										
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
<?php include('./../inc/footer.php');?>