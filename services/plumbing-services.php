<?php 
$page_title="Plumbing Services";
include('./../inc/header.php');
include('./../inc/partials/services-side-nav.php');
?>
<section class="page-banner" style="background-image:url(/images/background/3.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>Plumbing Services</h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
					<li><a href="/services.php">Services</a></li>
                    <li>Plumbing Services</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
	
	<!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
				<!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
                	<aside class="sidebar padding-right">
						
						<!-- Category Widget -->
                        <div class="sidebar-widget categories-two">
                            <div class="widget-content">
                                <!-- Services Category -->
                                <?php echo side_nav('plumbing-services');?>
                            </div>
                        </div>
						
						<!-- Brochures Widget -->
                        <div class="sidebar-widget brochures">
                            <div class="sidebar-title"><h5>Downloads</h5></div>
                            <div class="widget-content">
                                <a href="#" class="brochure-btn"><span class="icon fas fa-file-pdf"></span> Presentation PDF</a>
                                <a href="#" class="brochure-btn"><span class="icon fas fa-file-alt"></span> Brochure DOC</a>
                            </div>
						</div>
						
						<!-- Services Widget -->
                       				<?php include('../inc/partials/widgets.php');?>

						
					</aside>
				</div>
				
				<!--Content Side / Services Detail-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                	<div class="services-detail">
						<div class="inner-box">
							<div class="image">
								<img src="/images/resource/plumbing.jpg" alt="" />
							</div>
							<div class="lower-content">
								<!-- Title Box -->
								<div class="title-box">
									<div class="title">We are Airtech</div>
									<h2>Plumbing Services</h2>
								</div>
								<div class="bold-text">Our plumbing engineers have expertise in Execution of Plumbing jobs. 
								We have a “whole building” approach and are instrumental in influencing high water efficiency, energy, fire protection, pollution systems and a sustainable site. 
</div>
								<div class="">
	We are able to provide assessments, recommend alternatives, and thus provide high- functioning systems that meet the client’s very specific individual needs as well as assisting in conserving natural resources.

								We have a “whole building” approach and are instrumental in influencing high water efficiency, energy, fire protection, pollution systems and a sustainable site. 
								We are able to provide assessments, recommend alternatives, and thus provide high- functioning systems that meet the client’s very specific individual needs as well as assisting in conserving natural resources.

									<p>
</p>
									
<div class='text'>
								<h3>What we offer</h3>

</div>
									<!-- Default Two Column -->
									<div class="default-two-column">
											<p>
Our expertise covers a wide variety of jobs in Plumbing execution in internal and external plumbing system which includes:
</p>
										<div class="row clearfix">
										
											<div class="column col-lg-6 col-md-6 col-sm-12">
											
												<ul class="list-style-one">
<li>Execution of internal and external plumbing/sanitary system for all type of buildings according to Modern codes of practice</li>
<li>Execution of domestic water supply system.</li>
<li>Execution of Internal and external drainage system.</li>
<li>Execution of rain water/ storm water recharge well</li>

													
												</ul>
											</div>
											<div class="column col-lg-6 col-md-6 col-sm-12">
											
												<ul class="list-style-one">
<li>Execution of all types of Sewerage Treatment Plants (STP), Water Treatment Plants (WTP), & Effluent Treatment Plants (ETP) etc.
</li>
<li>Execution of Pump Room equipment (Hot and Cold water supply equipment).</li>
<li>Execution of all types of Modern Sanitary Fixtures works Execution of Steam, Sauna, Jacuzzi & Swimming pool works.</li>
													
													
												</ul>
											</div>
										</div>
									</div>
								</div>
								
								<h5>Our services at a glance ...</h5>
								<!-- Fact Counter -->
								<div class="fact-counter">
									<div class="clearfix">
										
										<!--Column-->
										<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
											<div class="inner wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
												<div class="count-outer count-box">
													<span class="count-text" data-speed="2000" data-stop="25">0</span>+
													<h4 class="counter-title">Years Experience</h4>
												</div>
											</div>
										</div>
										
										<!--Column-->
										<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
											<div class="inner wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
												<div class="count-outer count-box">
													<span class="count-text" data-speed="2500" data-stop="36">0</span>
													<h4 class="counter-title">Industries Served</h4>
												</div>
											</div>
										</div>
										
										<!--Column-->
										<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
											<div class="inner wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
												<div class="count-outer count-box">
													<span class="count-text" data-speed="3000" data-stop="105">0</span>
													<h4 class="counter-title">Factories Built</h4>
												</div>
											</div>
										</div>
										
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
<?php include('./../inc/footer.php');?>