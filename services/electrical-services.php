<?php 
$page_title="Electrical Services";
include('./../inc/header.php');
include('./../inc/partials/services-side-nav.php');
?>
<section class="page-banner" style="background-image:url(/images/background/3.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>Electrical Services</h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
					<li><a href="/services.php">Services</a></li>
                    <li>Electrical Services</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
	
	<!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
				<!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
                	<aside class="sidebar padding-right">
						
						<!-- Category Widget -->
                        <div class="sidebar-widget categories-two">
                            <div class="widget-content">
                                <!-- Services Category -->
                                <?php echo side_nav('electrical-services');?>
                            </div>
                        </div>
						
						<!-- Brochures Widget -->
                        <div class="sidebar-widget brochures">
                            <div class="sidebar-title"><h5>Downloads</h5></div>
                            <div class="widget-content">
                                <a href="#" class="brochure-btn"><span class="icon fas fa-file-pdf"></span> Presentation PDF</a>
                            </div>
						</div>
						
						<!-- Services Widget -->
										<?php include('../inc/partials/widgets.php');?>

                       
						
					</aside>
				</div>
				
				<!--Content Side / Services Detail-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                	<div class="services-detail">
						<div class="inner-box">
							<div class="image">
								<img src="/images/resource/electrical.jpg" alt="" />
							</div>
							<div class="lower-content">
								<!-- Title Box -->
								<div class="title-box">
									<div class="title">We are Airtech</div>
									<h2>Electrical Services</h2>
								</div>
								<div class="bold-text">Airtech has an expert team of highly competent and experienced electrical engineers. We are able to understand specific requirements of various institutional clients like hotels, resorts, hospitals, malls, industries etc. 
</div>
								<div class="">

									<p>Our team can closely work in various electrical activities involving in from point wiring, DB dressing, cabling, trunking, earthing, lightning protection, main panels, transformer etc. Our team is also qualified in ELV (Extra Low Voltage) work to execute fire alarm, PA system, CCTV, access control, MATV etc.
</p>
									
<div class='text'>
								<h3>What we offer</h3>

</div>
									<!-- Default Two Column -->
									<div class="default-two-column">
											<p>
													Our expertise in electrical services covers ample variety in the execution of both internal electrification as well external electrification work:
												</p>
										<div class="row clearfix">
										
											<div class="column col-lg-6 col-md-6 col-sm-12">
											
												<ul class="list-style-one">
<li>Execution of HT and LT feeders and cables</li>
<li>Execution of HT and LT panels, distribution boards</li>
<li>Preparation of Schematic and working drawings of internal and external schemes</li>
<li>Complete Installation of Internal & External Equipment including Internal Wiring, HT/ LT panels, Transformers and metering panels, Distribution boards, Risers, Tap off boxes and all types of ACB and VCB panels etc.</li>

													
												</ul>
											</div>
											<div class="column col-lg-6 col-md-6 col-sm-12">
											
												<ul class="list-style-one">
<li>Execution of conventional and Intelligent Fire Alarm and PA system</li>
<li>Execution of Lightning protection and complete earthing system for HT and LT equipment.</li>
<li>Execution of LV and IT network system, Access Control, MATV etc. Execution of intelligent external and street lightings</li>
													
													
												</ul>
											</div>
										</div>
									</div>
								</div>
								
								<h5>Our services at a glance ...</h5>
								<!-- Fact Counter -->
								<div class="fact-counter">
									<div class="clearfix">
										
										<!--Column-->
										<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
											<div class="inner wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
												<div class="count-outer count-box">
													<span class="count-text" data-speed="2000" data-stop="25">0</span>+
													<h4 class="counter-title">Years Experience</h4>
												</div>
											</div>
										</div>
										
										<!--Column-->
										<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
											<div class="inner wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
												<div class="count-outer count-box">
													<span class="count-text" data-speed="2500" data-stop="36">0</span>
													<h4 class="counter-title">Industries Served</h4>
												</div>
											</div>
										</div>
										
										<!--Column-->
										<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
											<div class="inner wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
												<div class="count-outer count-box">
													<span class="count-text" data-speed="3000" data-stop="105">0</span>
													<h4 class="counter-title">Factories Built</h4>
												</div>
											</div>
										</div>
										
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
<?php include('./../inc/footer.php');?>