<?php
$page_title = 'Telecom and Data centers';
include('./../inc/header.php');
include('./partials/sidenav.php');

?>


<section class="page-banner" style="background-image:url(/images/background/3.jpg);">
	<div class="auto-container">
		<div class="inner-container clearfix">
			<h1>Telecom and Data centers</h1>
			<ul class="bread-crumb clearfix">
				<li><a href="/">Home</a></li>
				<li><a href="#">Projects</a></li>
				<li>Telecom and Data centers</li>
			</ul>
		</div>
	</div>
</section>


<div class="sidebar-page-container">
	<div class="auto-container">
		<div class="row clearfix">

			<!--Sidebar Side-->
			<div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
				<aside class="sidebar padding-right">
						 <div class="sidebar-widget categories-two">
                            <div class="widget-content">
                                <!-- Services Category -->
                                <?php echo p_side_nav('telecom');?>
                            </div>
                        </div>

					<!-- Category Widget -->
				<?php include('../inc/partials/widgets.php');?>


					<!-- Brochures Widget -->
					<div class="sidebar-widget brochures">
						<div class="sidebar-title">
							<h5>Related Downloads</h5>
						</div>
						<div class="widget-content">
							<a href="#" class="brochure-btn"><span class="icon fas fa-file-pdf"></span> Brochure PDF</a>
						</div>
					</div>

					<!-- Services Widget -->


				</aside>
			</div>

			<!--Content Side / Services Detail-->
			<div class="content-side col-lg-8 col-md-12 col-sm-12">
				<div class="services-detail">
					<div class="inner-box">
						<div class="lower-content">
								<?php
							$images = [
								'/images/resource/projects/Telecom and Data Centres.jpg',
							

                                
							]
							?>
							<div class="image">
								<?php include('./../inc/partials/photogallery.php') ?>
							</div>
							<!-- Title Box -->
							<div class="title-box">
								<h2>Telecom and Data centers</h2>
							</div>
							<div class='bold-text'>
								Telecom companies have a very unique requirement of Air Conditioning. They need Air Conditioning for their equipment to perform at the desired level round the year. Airtech values this unique requirement and has been able to provide energy efficient, cost effective, eco friendly and sustainable solutions to the leading telecom operators and data centers in Nepal.

							</div>
							<div class="text">
								Unlike comfort Air Conditioning, telecom equipment needs cooling round the year and they have high sensible heat load. Accordingly the HVAC equipments selected must have higher sensible load capacity ensuring that the HVAC units meet the requirement of highly sensitive telecom equipments.
								<br>
								Data centers and server rooms are operating 24 X7 – 365 days and need to be designed for the round the year operation and also ensuring that there is zero failure. Accordingly Precision Type Air Conditioning of correct capacities with an additional stand by unit is strongly recommended at such sensitive installation. It is highly desirable to understand the amount of heat dissipation from the telecom servers to enable us to provide the correct temperature and relative humidity at these centers.
								<br>Airtech currently has the following product portfolio to support telecom/ Data Centers solutions-


							</div>
							<div class='pl-4 my-4'>
								<ul class='list-style-one'>
									<li>Green Telecom Shelters for BTS Sites.</li>
									<li>Telecom AC with direct free cooling for BTS sites.</li>
									<li>Heat Exchanger Units.</li>
									<li>Free Cooling - Fan coil units.</li>
									<li>Precision Type Air Conditioners for data centers & server rooms.</li>
									<li>Precision Type Air Conditioners with indirect free cooling.</li>
									<li>Commercial Air Conditioners with and without free cooling.</li>
									<li>Modular Access flooring.</li>
								</ul>
							</div>



							<div class='text'>

							</div>
							<!-- Default Two Column -->
							<div class="default-two-column">
								<div class='bold-text'>
									Airtech is very proud to be associated with the following companies to meet their air conditioning, shelter & access floor requirements.</div>


								<div class="row clearfix">

									<div class="column col-lg-6 col-md-6 col-sm-12">

										<ul class="list-style-two">
											<li>NCELL</li>
											<li>UNITED TELECOM LIMITED</li>
											<li>NEPAL TELECOM LTD.</li>
											<li>HOME TV (DTH).</li>
											<li>DATA HUB</li>
											<li>CLOUD HIMALAYA</li>
											<li>OHM Data Center</li>



										</ul>
									</div>

								</div>
							</div>


							<h5>At a glance ...</h5>
							<!-- Fact Counter -->
							<div class="fact-counter">
								<div class="clearfix">

									<!--Column-->
									<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
										<div class="inner wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2000" data-stop="25">0</span>+
												<h4 class="counter-title">Years Experience</h4>
											</div>
										</div>
									</div>

									<!--Column-->
									<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
										<div class="inner wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2500" data-stop="36">0</span>
												<h4 class="counter-title">Industries Served</h4>
											</div>
										</div>
									</div>

									<!--Column-->
									<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
										<div class="inner wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="3000" data-stop="105">0</span>
												<h4 class="counter-title">Factories Built</h4>
											</div>
										</div>
									</div>

								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>


<?php include('./../inc/footer.php'); ?>