<?php
$page_title = 'Banks and Corporate Institutions';
include('./../inc/header.php');
include('./partials/sidenav.php');

?>


<section class="page-banner" style="background-image:url(/images/background/3.jpg);">
    <div class="auto-container">
        <div class="inner-container clearfix">
            <h1>Banks and Corporate Institutions</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="/">Home</a></li>
                <li><a href="#">Projects</a></li>
                <li>Banks and Corporate Institutions</li>
            </ul>
        </div>
    </div>
</section>


<div class="sidebar-page-container">
    <div class="auto-container">
        <div class="row clearfix">

            <!--Sidebar Side-->
            <div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
                <aside class="sidebar padding-right">
                	
                							 <div class="sidebar-widget categories-two">
                            <div class="widget-content">
                                <!-- Services Category -->
                                <?php echo p_side_nav('corporate-institutions');?>
                            </div>
                        </div>

                    <!-- Category Widget -->
                   <?php include('../inc/partials/widgets.php');?>


                    <!-- Brochures Widget -->
                    <div class="sidebar-widget brochures">
                        <div class="sidebar-title">
                            <h5>Related Downloads</h5>
                        </div>
                        <div class="widget-content">
                          							<a href="#" class="brochure-btn"><span class="icon fas fa-file-pdf"></span> Brochure PDF</a>

                        </div>
                    </div>

                    <!-- Services Widget -->

                </aside>
            </div>

            <!--Content Side / Services Detail-->
            <div class="content-side col-lg-8 col-md-12 col-sm-12">
                <div class="services-detail">
                    <div class="inner-box">
                        <div class="lower-content">
                            <!-- Title Box -->
                            
                            <?php
							$images = [
								'/images/resource/projects/corporate1.png',
								'/images/resource/projects/corporate2.png',
								'/images/resource/projects/corporate3.png',
                                '/images/resource/projects/corporate4.png',

                                
							]
							?>
							<div class="image">
								<?php include('./../inc/partials/photogallery.php') ?>
							</div>
                            <div class="title-box">
                                <h2>Banks and Corporate Institutions</h2>
                            </div>
                            <div class="bold-text">

                                Airtech is proudly associated with Nepal’s leading commercial banks, financial institutions and corporate offices. We understand their specific requirement to provide the ultimate solution in terms of optimum performance, highest energy efficiency, maintaining aesthetics looks as per architect & interior designer.


                            </div>
                            <h3 class='mb-4'>
                                Basic requirements of banks and corporate institutions

                            </h3>

                            <div class='accordian-column'>
                                <ul class="accordion-box">
                                    <!--Block-->
                                    <li class="accordion block wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                                        <div class="acc-btn">
                                            <div class="icon-outer"><span class="icon icon-plus fa fa-angle-right"></span> </div>
                                            High Energy efficiency
                                        </div>
                                        <div class="acc-content">
                                            <div class="content">
                                                <div class="text">
                                                    As these institutes need to run the Air Conditioning System on all working days.
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <!--Block-->
                                    <li class="accordion block active-block wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                                        <div class="acc-btn active">
                                            <div class="icon-outer"><span class="icon icon-plus fa fa-angle-right"></span> </div>
                                            Extended Working Hours
                                        </div>
                                        <div class="acc-content current">
                                            <div class="content">
                                                <div class="text">

                                                    In some corporate offices there is a partial requirement of Air Conditioning even during extended working hours and on holidays.
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <!--Block-->
                                    <li class="accordion block wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                                        <div class="acc-btn">
                                            <div class="icon-outer"><span class="icon icon-plus fa fa-angle-right"></span> </div>
                                            Easy Compatibility with DG Sets
                                        </div>
                                        <div class="acc-content">
                                            <div class="content">
                                                <div class="text">

                                                    Considering the current energy crisis most of the AC units run on DG sets. Accordingly the A.C equipment must be selected which can operate with the DG.
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="accordion block wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                                        <div class="acc-btn">
                                            <div class="icon-outer"><span class="icon icon-plus fa fa-angle-right"></span> </div>
                                            Customer Service Areas
                                        </div>
                                        <div class="acc-content">
                                            <div class="content">
                                                <div class="text">

                                                    Faster air circulation & temperature control in customer service areas.
                                                </div>
                                            </div>
                                        </div>
                                    </li>


                                    <li class="accordion block wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                                        <div class="acc-btn">
                                            <div class="icon-outer"><span class="icon icon-plus fa fa-angle-right"></span> </div>
                                            Aesthetics
                                        </div>
                                        <div class="acc-content">
                                            <div class="content">
                                                <div class="text">


                                                    We need to plan the units which can ensure that the overall looks of the entire building both inside as well as outside is as per the expectation of the Architect / interior designers.
                                                </div>
                                            </div>
                                        </div>
                                    </li>


                                    <li class="accordion block wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                                        <div class="acc-btn">
                                            <div class="icon-outer"><span class="icon icon-plus fa fa-angle-right"></span> </div>
                                            Server Rooms
                                        </div>
                                        <div class="acc-content">
                                            <div class="content">
                                                <div class="text">

                                                    Unlike Comfort Air conditioning, server rooms need high sensible cooling and even need to operate in cooling mode in winters. Based on this and also based on heat dissipation suitable AC must be selected. It is also recommended to have a standby unit as the server rooms need cooling 24 X 7 – 365 days. </div>
                                            </div>
                                        </div>
                                    </li>


                                    <li class="accordion block wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                                        <div class="acc-btn">
                                            <div class="icon-outer"><span class="icon icon-plus fa fa-angle-right"></span> </div>
                                            Rated Available Power Supply
                                        </div>
                                        <div class="acc-content">
                                            <div class="content">
                                                <div class="text">

                                                    Some of the Banks / Corporate institutions do not have the required correct voltage to run the Air Conditioners. In such cases, we need to select the equipments which can operate at lower voltages or suggest the correct capacity of Voltage stabilizer depending upon the AC load & available power conditions at that site. </div>
                                            </div>
                                        </div>
                                    </li>


                                    <!--Block-->

                                </ul>

                            </div>


                            <div class='text'>
                                Based on all the various parameters suitable, the HVAC system must be designed and selected. Normally the most ideal system for High end corporate buildings is the VRV/VRF systems. It gives a great flexibility in the system design by allowing the selection of the units with the following major advantages.

                            </div>

                            <div class='pl-4 my-4'>
                                <ul class='list-style-one'>
                                    <li>Extended long piping lengths up to 1000 meters. </li>
                                    <li>Multiple indoor up to 64 Nos with a single outdoor.</li>
                                    <li>High Diversity indoor to outdoor ratio up to 200% depending upon the application. </li>
                                    <li>High Energy efficiency units leading to power saving up to 67 %.</li>
                                    <li>Extremely low sound pressure level ensuring whisper quiet operation.</li>
                                </ul>

                            </div>


                            <div class='title-box'>
                                <h3>Our valued Costumers</h3>

                            </div> <!-- Default Two Column -->
                            <div class="default-two-column">



                                <div class="row clearfix">

                                    <div class="column col-lg-6 col-md-6 col-sm-12">

                                        <ul class="list-style-two">

                                            <li>Ace Development Bank Ltd. </li>
                                            <li>Agni Incorporated Pvt. Ltd. </li>
                                            <li>Alliance Insurance Company Ltd.</li>
                                            <li>APCA Nepal Pvt. Ltd.</li>
                                            <li>Asian Life Insurance</li>
                                            <li>Atlas De Cargo</li>
                                            <li>Bank of Asia Ltd.</li>
                                            <li>Bank of Kathmandu Ltd.</li>
                                            <li>Birgunj Finance Ltd.</li>
                                            <li>Birgunj Town Hall</li>
                                            <li>Butwal Finance Ltd.</li>
                                            <li>Butwal Power Company</li>
                                            <li>Central Business Park</li>
                                            <li>Century Bank Ltd.</li>
                                            <li>Clean Energy Development Bank Ltd. </li>
                                            <li>CSC & CO Chartered Accountants </li>
                                            <li>Everest Bank Ltd.</li>
                                            <li>Everest Insurance Ltd.</li>
                                            <li>Ghorahi Cement
                                            <li>Global College</li>
                                            <li>Global IME Bank Ltd.</li>
                                            <li>H & B Development Bank Ltd. </li>
                                            <li>Hama Steels Pvt. Ltd. </li>
                                            <li>Himalayan Bank Ltd.</li>
                                            <li>Himalayan Builders Pvt. Ltd. </li>
                                            <li>Himchuli Bikash Bank Ltd. </li>
                                            <li>Janta Bank Ltd.</li>
                                            <li>Kantipur Publications</li>
                                            <li>Kist Bank Ltd.</li>
                                            <li>KL Tower</li>
                                            <li>Kumari Bank Ltd.</li>
                                            <li>Laxmi Bank Ltd.</li>
                                            <li>Lumbini Bank Ltd</li>
                                            <li>Vibor Bank Ltd.
                                            </li>
                                            <li>Web Search Pvt. Ltd.
                                            </li>
                                            <li>Yeti Development Bank Ltd.
                                            </li>






                                        </ul>
                                    </div>
                                    <div class="column col-lg-6 col-md-6 col-sm-12">

                                        <ul class="list-style-two">


                                            <li>Lumbini General Insurance Co. Ltd.
                                            </li>
                                            <li>Machhapuchhre Bank Ltd.
                                            </li>
                                            <li>Manakamana Development Bank Ltd.
                                            </li>
                                            <li>Mega Bank Ltd.
                                            </li>
                                            <li>Metro Park
                                            </li>
                                            <li>Morang Auto - Corporate Office
                                            </li>
                                            <li>Nabil Bank Ltd.
                                            </li>
                                            <li>National Life Insurance Co. Ltd.
                                            </li>
                                            <li>NB Bank Ltd.
                                            </li>
                                            <li>NCC Bank Ltd.
                                            </li>
                                            <li>Nepal Investment Bank Ltd.
                                            </li>
                                            <li>Nepal SBI Bank Ltd.
                                            </li>
                                            <li>Nimbus Pvt. Ltd.
                                            </li>
                                            <li>NMB Bank Ltd.
                                            </li>
                                            <li>NSET
                                            </li>
                                            <li>Prabhu Bank Ltd.
                                            </li>
                                            <li>Prime Commercial Bank Ltd.
                                            </li>
                                            <li>Prime Life Insurance
                                            </li>
                                            <li>Regency Watch
                                            </li>
                                            <li>Regus Business Centre Management Pvt. Ltd.
                                            </li>
                                            <li>Sanigad Hydro
                                            </li>
                                            <li>Sanima Bank Ltd.
                                            </li>
                                            <li>Sharda Group - Corporate Office
                                            </li>
                                            <li>Siddhartha Bank Ltd.
                                            </li>
                                            <li>Sino Sagarmatha Hydro Power Company
                                            </li>
                                            <li>Social Development Bank Ltd.
                                            </li>
                                            <li>SPI Nepal Pvt. Ltd.
                                            </li>
                                            <li>Standard Chartered Bank Ltd.
                                            </li>
                                            <li>Standard Finance Ltd.
                                            </li>
                                            <li>Sujal Group - Corporate Office
                                            </li>
                                            <li>Tele talk (P) Ltd.
                                            </li>
                                            <li>Toyota Showroom
                                            </li>
                                            <li>Trans Himalayan Express Pvt. Ltd.
                                            </li>
                                            <li>United Distributors Nepal Pvt. Ltd.
                                            </li>
                                            <li>Varun Developers
                                            </li>
                                            <li>Verisk Information Technologies
                                            </li>







                                        </ul>
                                    </div>
                                </div>
                            </div>


                            <h5>At a glance ...</h5>
                            <!-- Fact Counter -->
                            <div class="fact-counter">
                                <div class="clearfix">

                                    <!--Column-->
                                    <div class="column counter-column col-lg-4 col-md-4 col-sm-12">
                                        <div class="inner wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                                            <div class="count-outer count-box">
                                                <span class="count-text" data-speed="2000" data-stop="25">0</span>+
                                                <h4 class="counter-title">Years Experience</h4>
                                            </div>
                                        </div>
                                    </div>

                                    <!--Column-->
                                    <div class="column counter-column col-lg-4 col-md-4 col-sm-12">
                                        <div class="inner wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
                                            <div class="count-outer count-box">
                                                <span class="count-text" data-speed="2500" data-stop="36">0</span>
                                                <h4 class="counter-title">Industries Served</h4>
                                            </div>
                                        </div>
                                    </div>

                                    <!--Column-->
                                    <div class="column counter-column col-lg-4 col-md-4 col-sm-12">
                                        <div class="inner wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
                                            <div class="count-outer count-box">
                                                <span class="count-text" data-speed="3000" data-stop="105">0</span>
                                                <h4 class="counter-title">Factories Built</h4>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<?php include('./../inc/footer.php'); ?>