<?php
$page_title = 'Pharmaceuticals and Laboratories';
include('./../inc/header.php');
include('./partials/sidenav.php');

?>


<section class="page-banner" style="background-image:url(/images/background/3.jpg);">
	<div class="auto-container">
		<div class="inner-container clearfix">
			<h1>Pharmaceuticals and Laboratories</h1>
			<ul class="bread-crumb clearfix">
				<li><a href="/">Home</a></li>
				<li><a href="#">Projects</a></li>
				<li>Hotels and Resorts</li>
			</ul>
		</div>
	</div>
</section>


<div class="sidebar-page-container">
	<div class="auto-container">
		<div class="row clearfix">

			<!--Sidebar Side-->
			<div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
				<aside class="sidebar padding-right">

	 <div class="sidebar-widget categories-two">
                            <div class="widget-content">
                                <!-- Services Category -->
                                <?php echo p_side_nav('pharmaceuticals-and-laboratories');?>
                            </div>
                        </div>
					<!-- Category Widget -->
				<?php include('../inc/partials/widgets.php');?>


					<!-- Brochures Widget -->
					<div class="sidebar-widget brochures">
						<div class="sidebar-title">
							<h5>Related Downloads</h5>
						</div>
						<div class="widget-content">
													<a href="#" class="brochure-btn"><span class="icon fas fa-file-pdf"></span> Brochure PDF</a>

						</div>
					</div>

					<!-- Services Widget -->

				</aside>
			</div>

			<!--Content Side / Services Detail-->
			<div class="content-side col-lg-8 col-md-12 col-sm-12">
				<div class="services-detail">
					<div class="inner-box">
						<div class="lower-content">
								<?php
							$images = [
								'/images/resource/projects/Pharmaceuticals and Laborataries.jpg',
							

                                
							]
							?>
							<div class="image">
								<?php include('./../inc/partials/photogallery.php') ?>
							</div>
							<!-- Title Box -->
							<div class="title-box">
								<h2>Pharmaceuticals and Laboratories</h2>
							</div>
							<div class="text">
								Airtech has been associated with practically all the major pharmaceutical companies in Nepal.
								It has helped in maintaining critical parameters for the various process requirements extending from manufacturing of tablets, ointments, capsulation, liquids and sterile products. The pharmaceutical industry has to conform to WHO GMP standards as well as other stringent standards. Airtech is fully aware of validation parameters and can help its customer with conforming to the same.
								<br>
								<br>
								Our specially designed systems for the Pharmaceutical Industry offer -

							</div>

							<div class='pl-4 my-4'>
								<ul class='list-style-one'>
									<li> Maintenance of varied temperature, relative humidity and air changes in different areas such as preparation, manufacturing, packing and storage.</li>
									<li>High Levels of filtration such as High Efficiency Particulate (HEPA) filters to maintain the required air quality.</li>
									<li>Ability to handle peak load at the time of batch production and drop to lower levels when batch is completed.</li>

							</div>
							</p>

							<div class='text'>

							</div>
							<!-- Default Two Column -->
							<div class="default-two-column">
								<p>

									Airtech HVAC systems are designed to fulfil precise air-conditioning needs in terms of temperature, relative humidity and other parameters like pressure gradients, air changes, air filtration, air direction for different classes of cleanliness required in Pharma industries.

								</p>
								<div class='bold-text'>
									Some of our valued customers includes:
								</div>


								<div class="row clearfix">

									<div class="column col-lg-6 col-md-6 col-sm-12">

										<ul class="list-style-two">

											<li>Alive Pharmaceuticals (P) Ltd, Biratnagar</li>
											<li>Apex Pharmaceuticals (P) Ltd., Birgunj</li>
											<li>CTL Laboratories (P) Ltd., Kathmandu</li>
											<li>Deurali - Janta Pharmaceuticals (P) Ltd, Kathmandu</li>
											<li>K-Lab (P) Ltd, Kathmandu</li>
											<li>Magnus Pharmaceuticals (P) Ltd., Birgunj</li>
											<li>National Health Care (P) Ltd, Birgunj</li>
											<li>Nepal Pharmaceuticals Lab (P) Ltd., Birgunj</li>
											<li>Ohm Pharmaceuticals (P).Ltd., Kathmandu Panas Pharmaceuticals (P) Ltd., Nepalgunj</li>
											<li>Patanjali Ayurvedic Kendra Pvt. Ltd., Birgunj Quest Pharmaceuticals (P) Ltd., Birgunj</li>




										</ul>
									</div>
									<div class="column col-lg-6 col-md-6 col-sm-12">

										<ul class="list-style-two">
											<li>Simca Lab (P) Ltd, Kathmandu</li>
											<li>SR Drug Laboratories (P) Ltd., Kathmandu Time Pharmaceuticals (P) Ltd., Narayangarh</li>
											<li>Vijaydeep Laboratories (P) Ltd., Kathmandu</li>
											<li>Arya Pharma Lab (P) Ltd., Birgunj</li>
											<li>Bhaskar Herbaceuticals (P) Ltd., Birgunj</li>
											<li>BSL-3 - National Public Health Laboratory, Kathmandu</li>
											<li>Elder Universal Pharmaceuticals (P) Ltd., Bhairahawa Everest Pharmaceuticals (P) Ltd., Birgunj</li>
											<li>Florid Laboratories (P) Ltd., Kathmandu.</li>
											<li>GD Laboratories (P) Ltd., Birgunj</li>
											<li>Genetica Laboratories (P) Ltd., Birgunj</li>




										</ul>
									</div>
								</div>
							</div>


							<h5>At a glance ...</h5>
							<!-- Fact Counter -->
							<div class="fact-counter">
								<div class="clearfix">

									<!--Column-->
									<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
										<div class="inner wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2000" data-stop="25">0</span>+
												<h4 class="counter-title">Years Experience</h4>
											</div>
										</div>
									</div>

									<!--Column-->
									<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
										<div class="inner wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2500" data-stop="36">0</span>
												<h4 class="counter-title">Industries Served</h4>
											</div>
										</div>
									</div>

									<!--Column-->
									<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
										<div class="inner wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="3000" data-stop="105">0</span>
												<h4 class="counter-title">Factories Built</h4>
											</div>
										</div>
									</div>

								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>


<?php include('./../inc/footer.php'); ?>