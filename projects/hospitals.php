<?php
$page_title = 'Hospitals';
include('./../inc/header.php');
include('./partials/sidenav.php');

?>


<section class="page-banner" style="background-image:url(/images/background/3.jpg);">
	<div class="auto-container">
		<div class="inner-container clearfix">
			<h1>Hospitals</h1>
			<ul class="bread-crumb clearfix">
				<li><a href="/">Home</a></li>
				<li><a href="#">Projects</a></li>
				<li>Hospitals</li>
			</ul>
		</div>
	</div>
</section>


<div class="sidebar-page-container">
	<div class="auto-container">
		<div class="row clearfix">

			<!--Sidebar Side-->
			<div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
				<aside class="sidebar padding-right">

					<div class="sidebar-widget categories-two">
						<div class="widget-content">
							<!-- Services Category -->
							<?php echo p_side_nav('hospitals'); ?>
						</div>
					</div>

					<!-- Category Widget -->
					<div class="sidebar-widget services-widget">
						<?php include('../inc/partials/widgets.php'); ?>

						<!-- Brochures Widget -->
						<div class="sidebar-widget brochures">
							<div class="sidebar-title">
								<h5>Related Downloads</h5>
							</div>
							<div class="widget-content">
								<a href="#" class="brochure-btn"><span class="icon fas fa-file-pdf"></span> Brochure PDF</a>

							</div>
						</div>

						<!-- Services Widget -->

				</aside>
			</div>

			<!--Content Side / Services Detail-->
			<div class="content-side col-lg-8 col-md-12 col-sm-12">
				<div class="services-detail">
					<div class="inner-box">
						<div class="lower-content">

							<?php
							$images = [
								'/images/resource/projects/hospitals1.png',



							];
							?>
							<div class="image">
								<?php include('./../inc/partials/photogallery.php') ?>
							</div>
							<!-- Title Box -->
							<div class="title-box">
								<h2>Hospitals</h2>
							</div>
							<div class="bold-text">
								Airtech is a pioneer in hospital air-conditioning in Nepal. It perfectly understands the importance of air conditioning in the healthcare industry. Airtech has designed and installed air-conditioning systems for almost all the major hospitals in the country and is a key partner when it comes to air-conditioning of medical centers. We also have to our credit, supply and installation of operation theatre air-conditioning systems with laminar airflow pattern in several reputed medical facilities.


							</div>
							<div class='text'>
								Our HVAC Systems provide you with a comprehensive solution for all types of hospital requirements. Patient rooms, consulting rooms, OPDs and receptions require general comfort air-conditioning. Treatment areas such as ICU, operation theatres and post operation recovery rooms have stringent demands with regard to indoor air quality, airflow patterns, cross- contamination control and a germ-free environment. Diagnostic equipment areas like MRI, CATH-LAB, CT scan etc. require air-conditioning that ensures smooth and hassle-free functioning of high end and life saving medical equipment.
								<br><br>
								We consider all these unique requirements while offering expert healthcare solutions. We also provide high levels of filtration to maintain air quality levels and prevent the release of in-born pollutants outside.
								<br>
								Since air-conditioning in any hospital operates on a 24 x 7 basis, it becomes imperative that the equipment should not only be reliable but also energy efficient. Needless to say, our solutions can help you save upto 45% on your electricity bills.

							</div>




							<!-- Default Two Column -->
							<div class="default-two-column">

								<div class='bold-text'>
									Some of our valued customers includes:
								</div>


								<div class="row clearfix">

									<div class="column col-lg-6 col-md-6 col-sm-12">

										<ul class="list-style-two">

											<li>Ashwins Medical College & Hospital (P) Ltd.</li>
											<li>Blue Cross Hospital, Kathmandu</li>
											<li>BP Koirala Institute of Health and Sciences, Dharan BP Koirala Memorial Cancer Hospital, Narayangarh. Chirayu Hospital, Kathmandu</li>
											<li>Chitwan Medical College , Narayanagarh</li>
											<li>Ciwec Clinic, Kathmandu</li>
											<li>Ciwec Clinic, Pokhara</li>
											<li>Dhulikhel Hospital</li>
											<li>Gandaki Medical College, Pokhara</li>
											<li>Gautam Buddha Samudayik Hospital, Butwal Grande City Clinic, Kathmandu</li>
											<li>Grande International Hospital, Kathmandu</li>
											<li>K Lab, Kathmandu</li>
											<li>Kathmandu Medical College, Kathmandu</li>
											<li>KIST Medical College, Lalitpur</li>
											<li>Siddhi Poly Clinic and Path Lab, Kathmandu</li>
											<li>Teaching Hospital, Kathmandu</li>
											<li>Universal College of Medical Sciences, Bhairahawa.</li>
											<li>
												Vayodha Hospital, Kathmandu</li>





										</ul>
									</div>
									<div class="column col-lg-6 col-md-6 col-sm-12">

										<ul class="list-style-two">


											<li>Lahan Eye Hospital, Lahan</li>
											<li>Lumbini Medical College, Palpa</li>
											<li>Manipal Teaching Hospital, Pokhara</li>
											<li>Manmohan Cardiovascular and Teaching Hospital, </li>
											<li>Ktm Manmohan Memorial Institute of Health and Science,</li>
											<li>Ktm Mechi Eye Care Center, Birtamod</li>
											<li>National Medical College</li>
											<li>National Trauma Center, Bir Hospital, Kathmandu</li>
											<li>Nepal Cancer Hospital, Banke</li>
											<li>Nepal Cancer Hospital, Kathmandu</li>
											<li>Nepal Mediciti Hospital, Kathmandu</li>
											<li>Nepal Orthopedic Hospital, Kathmandu</li>
											<li>Nepalese Army Institute of Health and Sciences, </li>
											<li>
												Kathmandu Nepalgunj Medical College, Nepalgunj</li>
											<li>Norvic International Hospital, Kathmandu</li>
											<li>Om Hospital (P) Ltd, Kathmandu.</li>
											<li>Patan Hospital, Lalitpur</li>






										</ul>
									</div>
								</div>
							</div>


							<h5>At a glance ...</h5>
							<!-- Fact Counter -->
							<div class="fact-counter">
								<div class="clearfix">

									<!--Column-->
									<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
										<div class="inner wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2000" data-stop="25">0</span>+
												<h4 class="counter-title">Years Experience</h4>
											</div>
										</div>
									</div>

									<!--Column-->
									<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
										<div class="inner wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2500" data-stop="36">0</span>
												<h4 class="counter-title">Industries Served</h4>
											</div>
										</div>
									</div>

									<!--Column-->
									<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
										<div class="inner wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="3000" data-stop="105">0</span>
												<h4 class="counter-title">Factories Built</h4>
											</div>
										</div>
									</div>

								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>


<?php include('./../inc/footer.php'); ?>