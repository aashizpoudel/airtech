<?php
$page_title = 'Auditorium, Halls, Theatres & Studios';
include('./../inc/header.php');
include('./partials/sidenav.php');

?>


<section class="page-banner" style="background-image:url(/images/background/3.jpg);">
	<div class="auto-container">
		<div class="inner-container clearfix">
			<h1>Auditorium, Halls, Theatres & Studios</h1>
			<ul class="bread-crumb clearfix">
				<li><a href="/">Home</a></li>
				<li><a href="#">Projects</a></li>
				<li>Auditorium, Halls, Theatres & Studios</li>
			</ul>
		</div>
	</div>
</section>


<div class="sidebar-page-container">
	<div class="auto-container">
		<div class="row clearfix">

			<!--Sidebar Side-->
			<div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
				<aside class="sidebar padding-right">
					
					
						 <div class="sidebar-widget categories-two">
                            <div class="widget-content">
                                <!-- Services Category -->
                                <?php echo p_side_nav('auditorium-halls-theaters-studios');?>
                            </div>
                        </div>

					<!-- Category Widget -->
					<?php include('../inc/partials/widgets.php');?>


					<!-- Brochures Widget -->
					<div class="sidebar-widget brochures">
						<div class="sidebar-title">
							<h5>Related Downloads</h5>
						</div>
						<div class="widget-content">
							<a href="#" class="brochure-btn"><span class="icon fas fa-file-pdf"></span> Booklet PDF</a>
						</div>
					</div>


					<!-- Services Widget -->

				</aside>
			</div>

			<!--Content Side / Services Detail-->
			<div class="content-side col-lg-8 col-md-12 col-sm-12">
				<div class="services-detail">
					<div class="inner-box">
						<div class="lower-content">
						<?php
							$images = [
								'/images/resource/projects/studio1.png',
								'/images/resource/projects/studio2.png',
								
							]
							?>
							<div class="image">
								<?php include('./../inc/partials/photogallery.php') ?>
							</div>
							<!-- Title Box -->
							<div class="title-box">
								<h2>Auditorium, Halls, Theatres & Studios</h2>
							</div>
							<div class="text">
								Airtech understands the specific requirement of different types of auditorium halls, theatres , studios, recording studio, FM studio, lecture halls etc. Our system design includes the following major criteria.
								<br><br>There is a specific peak load demand in the auditorium for a short time and the desired installed HVAC system must be able to take care of the same.
								<br>Noise is a very important criteria specially for recording studios, lecture halls & theatres. Based on the acceptable noise level Airtech is able to design the ducts with suitable acoustic insulation to meet the most stringent noise levels.
								<br>Fresh Air & proper air exhaust is also a major criteria for large halls and Airtech installed systems ensure that required level of fresh air is incorporated as per the specific requirements.
								<br>The equipment and light loads in some special studios.
								<br>The interior aesthetics of the hall along with proper air distribution is also an important parameter considered by Airtech for the design of auditorium halls & theatres.



							</div>


							</p>

							<div class='text'>

							</div>
							<!-- Default Two Column -->
							<div class="default-two-column">
								<p>

									Since the air conditioning constitutes a major portion of electricity consumption in the hotels, energy efficiency is of vital importance. Needless to say, Airtech design and solutions can save up to 45% on electricity cost.
									<br>Taking all the above requirements into consideration enables us to offer you a comprehensive solution for your hotel and ensure that your guests keep coming back to you.

								</p>
								<div class='bold-text'>
									Some of our valued customers includes:
								</div>


								<div class="row clearfix">

									<div class="column col-lg-6 col-md-6 col-sm-12">

										<ul class="list-style-two">

											<li>Avenues Television</li>
											<li>Kumari Hall Auditorium</li>
											<li>Big Cinemas</li>
											<li>DAV School - Auditorium Hall</li>
											<li>F Cube Cinema</li>
											<li>Indradev Chalchitra Mandir, Chitwan</li>
											<li>Jai Nepal Hall</li>
											<li>Kantipur Television</li>
											<li>Lecture Hall – Kist Medical</li>
											<li>Lecture Hall - UCMS</li>




										</ul>
									</div>
									<div class="column col-lg-6 col-md-6 col-sm-12">

										<ul class="list-style-two">
											<li>Lincoln School</li>
											<li>Movies Entertainment, Narayanghat</li>
											<li>Nach Ghar</li>
											<li>Narayani Mall, Hetauda</li>
											<li>NATHM College - Auditorium Hall</li>
											<li>Premier School - Auditorium Hall</li>
											<li>QFX- Civil Mall</li>
											<li>Rato Bangla School - Auditorium Hall</li>
											<li>St. Mary’s School - Auditorium Hall</li>
											<li>Damak Chitra Mandir, Damak</li>



										</ul>
									</div>
								</div>
							</div>


							<h5>At a glance ...</h5>
							<!-- Fact Counter -->
							<div class="fact-counter">
								<div class="clearfix">

									<!--Column-->
									<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
										<div class="inner wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2000" data-stop="25">0</span>+
												<h4 class="counter-title">Years Experience</h4>
											</div>
										</div>
									</div>

									<!--Column-->
									<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
										<div class="inner wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2500" data-stop="36">0</span>
												<h4 class="counter-title">Industries Served</h4>
											</div>
										</div>
									</div>

									<!--Column-->
									<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
										<div class="inner wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="3000" data-stop="105">0</span>
												<h4 class="counter-title">Factories Built</h4>
											</div>
										</div>
									</div>

								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>


<?php include('./../inc/footer.php'); ?>