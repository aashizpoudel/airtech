<?php
$page_title = 'Industries';
include('./../inc/header.php');

include('./partials/sidenav.php');

?>


<section class="page-banner" style="background-image:url(/images/background/3.jpg);">
	<div class="auto-container">
		<div class="inner-container clearfix">
			<h1>Industries</h1>
			<ul class="bread-crumb clearfix">
				<li><a href="/">Home</a></li>
				<li><a href="#">Projects</a></li>
				<li>Industries</li>
			</ul>
		</div>
	</div>
</section>


<div class="sidebar-page-container">
	<div class="auto-container">
		<div class="row clearfix">

			<!--Sidebar Side-->
			<div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
				<aside class="sidebar padding-right">

					<div class="sidebar-widget categories-two">
						<div class="widget-content">
							<!-- Services Category -->
							<?php echo p_side_nav('industries'); ?>
						</div>
					</div>

					<!-- Category Widget -->
					<?php include('../inc/partials/widgets.php'); ?>

					<!-- Brochures Widget -->
					<div class="sidebar-widget brochures">
						<div class="sidebar-title">
							<h5>Related Downloads</h5>
						</div>
						<div class="widget-content">
							<a href="#" class="brochure-btn"><span class="icon fas fa-file-pdf"></span> Brochure PDF</a>

						</div>
					</div>

					<!-- Services Widget -->


				</aside>
			</div>

			<!--Content Side / Services Detail-->
			<div class="content-side col-lg-8 col-md-12 col-sm-12">
				<div class="services-detail">
					<div class="inner-box">
						<div class="lower-content">
							<?php
							$images = [
								'/images/resource/projects/industries1.png',



							]
							?>
							<div class="image">
								<?php include('./../inc/partials/photogallery.php') ?>
							</div>
							<!-- Title Box -->
							<div class="title-box">
								<h2>Industries</h2>
							</div>
							<div class='text'>
								Heating, Ventilation & Air Conditioning requirement varies in different industries as per their specific requirement.
								It can be a simple comfort air conditioning for a manager room to an industrial process chilling for sensitive requirements.
								Air conditioning is required to cool a sensitive panel board which dissipates heat.
								Sometimes in industries even fresh air and an exhaust system is installed to ensure air movement in warehousing to
								ensure that there is no microbial growth. However in some warehouses there is a requirement of Air Conditioning if
								the products properties degrade in the adverse climate.
								<br>
							</div>
							<div class='text'>
								Air conditioning is required to cool a sensitive panel board which dissipates heat.
								Sometimes in industries even fresh air and an exhaust system is installed to ensure air movement in warehousing to
								ensure that there is no microbial growth. However in some warehouses there is a requirement of Air Conditioning if
								the products properties degrade in the adverse climate.
								<br>
								Industrial air conditioning must ensure that there is good know-how of the actual process so that the designed HVAC can provide the required temperature control. Airtech values the requirement of each industry and designs the system with the feedback from the production managers enabling them to meet their process requirement. Sometimes there is a concern of extreme outside conditions which needs also to be addressed. Airtech provides the solution by using water cooled package units in such extreme environments so that the AC units life is extended as compared to the air cooled outdoor units.


							</div>
							<br><br>
							<!-- Default Two Column -->
							<div class="default-two-column">
								<div class='bold-text'>
									Some of the leading Industries in Nepal are using our HVAC system -


									<div class="row clearfix mt-3">

										<div class="column col-lg-6 col-md-6 col-sm-12">

											<ul class="list-style-two">
												<li>Asian Thai Food (P) Ltd., Biratnagar</li>
												<li>Avinash Hatcheries (P) Ltd., Narayangarh</li>
												<li>Bottlers Nepal Ltd., Narayangarh</li>
												<li>Dabur Nepal (P) Ltd., Birgunj</li>
												<li>Gorkha Breweries (P) Ltd., Narayangarh</li>
												<li>Hulas Steel Industries Ltd., Birgunj</li>
												<li>Jyoti Group, Birgunj</li>
												<li>Nepal Wellhope Agri-Tech (P) Ltd., Chitwan</li>
												<li>Panchakanya Group., Bhairahawa,</li>
												<li>Kathmandu Probiotech Industries (P) Ltd., Birgunj</li>
												<li>Sujal Foods (P) Ltd., Pokhara</li>
												<li>Sumy Distillery (P) Ltd., Narayangarh</li>
												<li>Surya Nepal (P) Ltd., Biratnagar</li>
												<li>Varun Beverages (P) Ltd., Kathmandu</li>




											</ul>
										</div>

										<div class="column col-lg-6 col-md-6 col-sm-12">

											<ul class="list-style-two">
												<li>Hama Iron & Steels Industries Pvt. Ltd.,</li>
												<li>Birgunj Jagdamba Steels Pvt. Ltd., Birgunj</li>
												<li>Jagdamba Foods Pvt. Ltd., Bhairahawa</li>
												<li>Viju Poultry Pvt. Ltd., Biratnagar</li>
												<li>Kathmandu Probiotech Industries (P) Ltd., Birgunj</li>
												<li>Sujal Foods (P) Ltd., Pokhara</li>
												<li>Sumy Distillery (P) Ltd., Narayangarh</li>
												<li>Surya Nepal (P) Ltd., Biratnagar</li>
												<li>Varun Beverages (P) Ltd., Kathmandu</li>
												<li>Hama Iron & Steels Industries Pvt. Ltd.,</li>
												<li>Birgunj Jagdamba Steels Pvt. Ltd., Birgunj</li>
												<li>Jagdamba Foods Pvt. Ltd., Bhairahawa</li>
												<li>Viju Poultry Pvt. Ltd., Biratnagar</li>



											</ul>
										</div>

									</div>
								</div>


								<h5>At a glance ...</h5>
								<!-- Fact Counter -->
								<div class="fact-counter">
									<div class="clearfix">

										<!--Column-->
										<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
											<div class="inner wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
												<div class="count-outer count-box">
													<span class="count-text" data-speed="2000" data-stop="25">0</span>+
													<h4 class="counter-title">Years Experience</h4>
												</div>
											</div>
										</div>

										<!--Column-->
										<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
											<div class="inner wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
												<div class="count-outer count-box">
													<span class="count-text" data-speed="2500" data-stop="36">0</span>
													<h4 class="counter-title">Industries Served</h4>
												</div>
											</div>
										</div>

										<!--Column-->
										<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
											<div class="inner wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
												<div class="count-outer count-box">
													<span class="count-text" data-speed="3000" data-stop="105">0</span>
													<h4 class="counter-title">Factories Built</h4>
												</div>
											</div>
										</div>

									</div>
								</div>

							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>


	<?php include('./../inc/footer.php'); ?>