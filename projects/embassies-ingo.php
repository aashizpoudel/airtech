<?php
$page_title = 'Embassies and International NGOs';
include('./../inc/header.php');
include('./partials/sidenav.php');

?>


<section class="page-banner" style="background-image:url(/images/background/3.jpg);">
	<div class="auto-container">
		<div class="inner-container clearfix">
			<h1>Embassies and International NGOs</h1>
			<ul class="bread-crumb clearfix">
				<li><a href="/">Home</a></li>
				<li><a href="#">Projects</a></li>
				<li>Embassies and International NGOs</li>
			</ul>
		</div>
	</div>
</section>


<div class="sidebar-page-container">
	<div class="auto-container">
		<div class="row clearfix">

			<!--Sidebar Side-->
			<div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
				<aside class="sidebar padding-right">
					
						 <div class="sidebar-widget categories-two">
                            <div class="widget-content">
                                <!-- Services Category -->
                                <?php echo p_side_nav('embassies-ingo');?>
                            </div>
                        </div>

					<!-- Category Widget -->
				<?php include('../inc/partials/widgets.php');?>


					<!-- Brochures Widget -->
					<div class="sidebar-widget brochures">
						<div class="sidebar-title">
							<h5>Related Downloads</h5>
						</div>
						<div class="widget-content">
													<a href="#" class="brochure-btn"><span class="icon fas fa-file-pdf"></span> Brochure PDF</a>

						</div>
					</div>

					<!-- Services Widget -->

				</aside>
			</div>

			<!--Content Side / Services Detail-->
			<div class="content-side col-lg-8 col-md-12 col-sm-12">
				<div class="services-detail">
					<div class="inner-box">
						<div class="lower-content">
							
								<?php
							$images = [
								'/images/resource/projects/Embassies and Internationals NGO\'s.jpg',
							

                                
							]
							?>
							<div class="image">
								<?php include('./../inc/partials/photogallery.php') ?>
							</div>
							<!-- Title Box -->
							<div class="title-box">
								<h2>Embassies and International NGOs</h2>
							</div>





							<!-- Default Two Column -->
							<div class="default-two-column">

								<div class='bold-text'>
									Airtech is proudly associated with many Embassies, International NGOs and foreign offices in Nepal for the Heating, Ventilation and AC Requirements.</div>


								<div class="row clearfix">

									<div class="column col-lg-6 col-md-6 col-sm-12">

										<ul class="list-style-two">

											<li>American Embassy</li>
											<li>British Embassy</li>
											<li>British Gurkhas</li>
											<li>DANIDA</li>
											<li>Danish Embassy</li>
											<li>Embassy of Saudi Arabia</li>
											<li>GIZ</li>
											<li>Indian Welfare</li>
											<li>IOM</li>
											<li>JICA</li>
											<li>Libird</li>
											<li>Nick Simons Institute</li>






										</ul>
									</div>
									<div class="column col-lg-6 col-md-6 col-sm-12">

										<ul class="list-style-two">


											<li>NSET</li>
											<li>Plan International</li>
											<li>Russian Embassy</li>
											<li>Save The Children</li>
											<li>Swiss Embassy</li>
											<li>UNDP</li>
											<li>UNFPA</li>
											<li>US Peace Corps</li>
											<li>WFO</li>
											<li>World Bank</li>
											<li>World Claim</li>
											<li>World Vision International</li>






										</ul>
									</div>
								</div>
							</div>


							<h5>At a glance ...</h5>
							<!-- Fact Counter -->
							<div class="fact-counter">
								<div class="clearfix">

									<!--Column-->
									<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
										<div class="inner wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2000" data-stop="25">0</span>+
												<h4 class="counter-title">Years Experience</h4>
											</div>
										</div>
									</div>

									<!--Column-->
									<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
										<div class="inner wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2500" data-stop="36">0</span>
												<h4 class="counter-title">Industries Served</h4>
											</div>
										</div>
									</div>

									<!--Column-->
									<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
										<div class="inner wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="3000" data-stop="105">0</span>
												<h4 class="counter-title">Factories Built</h4>
											</div>
										</div>
									</div>

								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>


<?php include('./../inc/footer.php'); ?>