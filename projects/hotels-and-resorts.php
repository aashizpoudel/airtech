<?php
$page_title = 'Hotels and Resorts';
include('./../inc/header.php');
include('./partials/sidenav.php');

?>


<section class="page-banner" style="background-image:url(/images/background/3.jpg);">
	<div class="auto-container">
		<div class="inner-container clearfix">
			<h1>Hotels and Resorts</h1>
			<ul class="bread-crumb clearfix">
				<li><a href="/">Home</a></li>
				<li><a href="#">Projects</a></li>
				<li>Hotels and Resorts</li>
			</ul>
		</div>
	</div>
</section>


<div class="sidebar-page-container">
	<div class="auto-container">
		<div class="row clearfix">

			<!--Sidebar Side-->
			<div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
				<aside class="sidebar padding-right">

					<div class="sidebar-widget categories-two">
						<div class="widget-content">
							<!-- Services Category -->
							<?php echo p_side_nav('hotels-and-resorts'); ?>
						</div>
					</div>

					<!-- Category Widget -->
					<?php include('../inc/partials/widgets.php'); ?>


					<!-- Brochures Widget -->
					<div class="sidebar-widget brochures">
						<div class="sidebar-title">
							<h5>Related Downloads</h5>
						</div>
						<div class="widget-content">
							<a href="#" class="brochure-btn"><span class="icon fas fa-file-pdf"></span> Brochure PDF</a>

						</div>
					</div>

					<!-- Services Widget -->


				</aside>
			</div>

			<!--Content Side / Services Detail-->
			<div class="content-side col-lg-8 col-md-12 col-sm-12">
				<div class="services-detail">
					<div class="inner-box">
						<div class="lower-content">
							<?php
							$images = [
								'/images/resource/projects/hotels1.png',
								'/images/resource/projects/hotels2.png',
								'/images/resource/projects/hotels3.png',
								
							]
							?>
							<div class="image">
								<?php include('./../inc/partials/photogallery.php') ?>
							</div>
							<!-- Title Box -->
							<div class="title-box">
								<h2>Hotels and Resorts</h2>
							</div>
							<div class="text">
								Airtech understands the characteristics and demands of air-conditioning in hotels. Our target is to provide a comfortable ambience to the guests round the year in different areas including guest rooms, restaurant, lobby, health club, banquet hall, casino, discotheque etc. Each of the above areas has its distinct requirement and we design the system based on the precise requirement of the different areas.

								<br>
								<br>
								Our design takes into account some very important factors in hotels like-

							</div>

							<div class='pl-4 my-4'>
								<ul class='list-style-one'>
									<li>Partial load due to varying occupancy rates.</li>
									<li>Higher load at more occupied zones like banquet halls, restaurants’ etc.</li>
									<li>Frequent door openings in the reception areas.</li>
									<li>Ventilation to utility areas such as kitchen, toilets, car parking’s etc. Low noise level in the rooms.</li>
									<li>High Energy efficiency.</li>
									<li>Aesthetics of the hotel.</li>
							</div>
							</p>

							<div class='text'>

							</div>
							<!-- Defaultt Two Column -->
							<div class="default-two-column">
								<p>

									Since the air conditioning constitutes a major portion of electricity consumption in the hotels, energy efficiency is of vital importance. Needless to say, Airtech design and solutions can save up to 45% on electricity cost.
									<br>Taking all the above requirements into consideration enables us to offer you a comprehensive solution for your hotel and ensure that your guests keep coming back to you.

								</p>
								<div class='bold-text'>
									Some of our valued customers includes:
								</div>


								<div class="row clearfix">

									<div class="column col-lg-6 col-md-6 col-sm-12">

										<ul class="list-style-two">

											<li>Ambassador Hotel, Kathmandu</li>
											<li>Annapurna Holdings Lodge</li>
											<li>Casino Grand, Pokhara</li>
											<li>Casino Rad- Hotel Radisson</li>
											<li>Casino Venus- Hotel Malla</li>
											<li>Central Palm Hotel, Chitwan</li>
											<li>Club Himalaya, Nagarkot</li>
											<li>Fairfield by Marriott, Kathmandu</li> 
											<li>Fish Tail Lodge Resort, Pokhara</li> 
											<li>Fulbari Resort, Pokhara</li>
											<li>Gokarna Forest Resort, Kathmandu
											</li><li>
											Hotel Annapurna, Kathmandu</li>
											<li>Hotel Barahi, Pokhara</li>
											<li>Hotel Garden, Chitwan</li>
											<li>Hotel Harmika, Bouddha</li>
											<li>Hotel Himalaya, Lalitpur</li>
											<li>Hotel Indreni Himalaya, Kathmandu </li>
											<li>Hotel Kalpataru, Nepalgunj</li>
											<li>Soaltee Crowne Plaza, Kathmandu</li>
											<li>Summit Hotel, Lalitpur</li>
											<li>The Dwarika’s Hotel, Kathmandu</li>
											<li> The Leaf Hospitality</li>



										</ul>
									</div>
									<div class="column col-lg-6 col-md-6 col-sm-12">

										<ul class="list-style-two">
											<li>Hotel Lumbini Kasai </li><li>Hotel Nova Boutique</li>
											<li>Hotel Radisson, Kathmandu</li>
											<li>Hotel Royal Century, Chitwan</li>
											<li>Hotel Shambala, Kathmandu</li>
											<li>Hotel Shangri-La, Kathmandu</li>
											<li>Hotel Speedway, Kathmandu</li>
											<li>Hotel Temple Tree, Gaushala</li>
											<li>Hotel Temple Tree, Pokhara</li>
											<li>Hotel Tibet (P) Ltd, Kathmandu</li><li>International Hotel Pvt. Ltd, Kathmandu</li>
											<li>Kasara Jungle Resort, Chitwan</li>
											<li>Lake View Resort, Pokhara </li>
											<li> Shalimar Hospitality, Kathmandu</li>
											<li> Siddhartha Hospitality Group, Nagarkot </li>

											<li>Tiger One Pvt. Ltd., Bhairahawa </li>
											<li>Trek O’Tel, Pokhara</li>
											<li>Yellow Pagoda Hotel</li>
											<li>Hotel Harrison Palace, Biratnagar</li>



										</ul>
									</div>
								</div>
							</div>


							<h5>At a glance ...</h5>
							<!-- Fact Counter -->
							<div class="fact-counter">
								<div class="clearfix">

									<!--Column-->
									<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
										<div class="inner wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2000" data-stop="25">0</span>+
												<h4 class="counter-title">Years Experience</h4>
											</div>
										</div>
									</div>

									<!--Column-->
									<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
										<div class="inner wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2500" data-stop="36">0</span>
												<h4 class="counter-title">Industries Served</h4>
											</div>
										</div>
									</div>

									<!--Column-->
									<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
										<div class="inner wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="3000" data-stop="105">0</span>
												<h4 class="counter-title">Factories Built</h4>
											</div>
										</div>
									</div>

								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>


<?php include('./../inc/footer.php'); ?>