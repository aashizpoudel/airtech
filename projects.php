<?php
$page_title = 'Our Projects';
include('inc/header.php'); ?>
<!--Page Title-->
<section class="page-banner style-two" style="background-image:url(images/background/services-title-bg.jpg);">
    <div class="auto-container">
        <div class="inner-container clearfix">
            <h1>Our Projects</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="index-2.html">Home</a></li>
                <li>Our Projects</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->

<!-- Services Section Three -->
<section class="services-section-three style-two">
    <div class="auto-container">
        <!-- Sec Title -->
        <div class="sec-title">
            <div class="row">
                <div class="col-lg-4 col-md-12 wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="title">We are Airtech</div>
                    <h2>Our Projects</h2>
                </div>
                <div class="col-lg-8 col-md-12 wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="text" style='margin-top:10px;margin-bottom:50px'>

                        Airtech’s projects are a one stop solution for the construction fundamentals into systematic, comprehensive and collaborated solutions for effective operation. The integration between the diverse techniques and procedures in the mechanical, electrical and plumbing, MEP resolves the complexity and transforms into a synergy and success.
                       <br><br>
                        From the start of any project, we understand the need to develop a building where the internal environment is balanced, providing comfortable spaces for occupants while keeping the running costs as low as possible. Achieving this requires an integrated approach, working with all members to ensure that all the systems are strategically integrated into the project.


                    </div>
                </div>
            </div>

            <div class="row mt-4">

                <!-- Services Block Two -->
                <div class="services-block-three col-xl-4 col-md-6">
                    <div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="/projects/hotels-and-resorts.php"><img src="images/resource/projects/Hotels & Resorts.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="/projects/hotels-and-resorts.php">Hotels and Resorts</a></h3>
                            <div class="text mt-4 mb-1">
                                Airtech understands the characteristics and demands of air-conditioning in hotels. Our target is to provide a comfortable ambience to the guests round the year in different areas including guest rooms, restaurant, lobby, health club, banquet hall, casino, discotheque etc.
                            </div>
                            <a href="/projects/hotels-and-resorts.php" class="read-more">Read more <span class="arrow fas fa-angle-right"></span></a>


                        </div>
                    </div>
                </div>

                <!-- Services Block Two -->
                <div class="services-block-three col-xl-4 col-md-6">
                    <div class="inner-box wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="/projects/pharmaceuticals-and-laboratories.php"><img style="height: 203px;width: 327px;"  src="images/resource/projects/Pharmaceuticals and Laborataries.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="/projects/pharmaceuticals-and-laboratories.php">Pharmaceuticals and Laboratories</a></h3>
                            <div class="text mt-4 mb-1">
                                Airtech has been associated with practically all the major pharmaceutical companies in Nepal. It has helped in maintaining critical parameters for the various process requirements extending from manufacturing of tablets, ointments, capsulation, liquids and sterile products


                            </div>
                            <a href="/projects/pharmaceuticals-and-laboratories.php" class="read-more">Read more <span class="arrow fas fa-angle-right"></span></a>


                        </div>
                    </div>
                </div>

                <!-- Services Block Two -->
                <div class="services-block-three col-xl-4 col-md-6">
                    <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="/projects/banks-and-corporate-institutions.php"><img style="height: 203px;width: 327px;" src="images/resource/projects/Banks and Institutions.JPG" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="/projects/banks-and-corporate-institutions.php">Banks and Corporate Institutions</a></h3>
                            <div class="text mt-4 mb-1">

                                Airtech is proudly associated with Nepal’s leading commercial banks, financial institutions and corporate offices. We understand their specific requirement to provide the ultimate solution in terms of optimum performance, highest energy efficiency, maintaining aesthetics as per architect & interior designer.
                            </div>
                            <a href="/projects/banks-and-corporate-institutions.php" class="read-more">Read more <span class="arrow fas fa-angle-right"></span></a>

                        </div>
                    </div>
                </div>

                <!-- Services Block Two -->
                <div class="services-block-three col-xl-4 col-md-6">
                    <div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="/projects/hospitals.php"><img src="images/resource/projects/Hospitals.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="/projects/hospitals.php">Hospitals</a></h3>
                            <div class="text mt-4 mb-1">Airtech is a pioneer in hospital air-conditioning in Nepal. It perfectly understands the importance of air conditioning in the healthcare industry. Airtech has designed and installed air-conditioning systems for almost all the major hospitals in the country and is a key partner when it comes to air-conditioning of medical centers</div>
                            <a href="/projects/hospitals.php" class="read-more">Read more <span class="arrow fas fa-angle-right"></span></a>


                        </div>
                    </div>
                </div>

                <!-- Services Block Two -->
                <div class="services-block-three col-xl-4 col-md-6">
                    <div class="inner-box wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="/projects/industries.php"><img src="images/resource/projects/Industries.JPG" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="/projects/industries.php">Industries</a></h3>
                            <div class="text mt-4 mb-1">
                                Airtech values the requirement of each industry and designs the system with the feedback from the production managers enabling them to meet their process requirement.
                            </div>
                            <a href="/projects/industries.php" class="read-more">Read more <span class="arrow fas fa-angle-right"></span></a>

                        </div>
                    </div>
                </div>

                <div class="services-block-three col-xl-4 col-md-6">
                    <div class="inner-box wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="/projects/auditorium-halls-theaters-studios.php"><img src="images/resource/projects/Auditorium, Halls, Theatres and Studios.JPG" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="/projects/auditorium-halls-theaters-studios.php">Auditorium, Halls, Theatres and Studios</a></h3>
                            <div class="text mt-4 mb-1">

                                Airtech understands the specific requirement of different types of auditorium halls, theatres , studios, recording studio, FM studio, lecture halls etc.
                                The interior aesthetics of the hall along with proper air distribution is also an important parameter considered by Airtech for the design of auditorium halls & theatres.
                            </div>
                            <a href="/projects/auditorium-halls-theaters-studios.php" class="read-more">Read more <span class="arrow fas fa-angle-right"></span></a>

                        </div>
                    </div>
                </div>

                <div class="services-block-three col-xl-4 col-md-6">
                    <div class="inner-box wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="/projects/embassies-ingo.php"><img src="images/resource/projects/Embassies and Internationals NGO's.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="/projects/embassies-ingo.php">Embassies and International NGOs</a></h3>
                            <div class="text mt-4 mb-1">
                                Airtech is proudly associated with many Embassies, International NGOs and foreign offices in Nepal for the Heating, Ventilation and AC Requirements.
                            </div>
                            <a href="/projects/embassies-ingo.php" class="read-more">Read more <span class="arrow fas fa-angle-right"></span></a>

                        </div>
                    </div>
                </div>


                <div class="services-block-three col-xl-4 col-md-6">
                    <div class="inner-box wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="/projects/telecom.php"><img src="images/resource/projects/Telecom and Data Centres.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="/projects/telecom.php">Telecom and Data centers</a></h3>
                            <div class="text mt-4 mb-1">A
                                Telecom companies have a very unique requirement of Air Conditioning. They need Air Conditioning for their equipment to perform at the desired level round the year. Airtech values this unique requirement and has been able to provide energy efficient, cost effective, eco friendly and sustainable solutions to the leading telecom operators and data centers in Nepal.
                            </div>
                            <a href="/projects/telecom.php" class="read-more">Read more <span class="arrow fas fa-angle-right"></span></a>

                        </div>
                    </div>
                </div>



            </div>
        </div>
</section>
<!-- End Services Section Three -->

<!-- Skill Section -->

<!-- End Skill Section -->

<?php include('inc/partials/testimonials.php'); ?>
<!-- Sponsors Section -->
<section class="sponsors-section alternate-2">
    <div class="auto-container">
        <?php include('inc/partials/clients.php'); ?>
    </div>
</section>
<!-- End Sponsors Section -->

<?php include('inc/footer.php'); ?>