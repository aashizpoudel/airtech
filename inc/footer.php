<!--Newsleter Section-->
<div style='height:60px'></div>

<!--End Newsleter Section-->

<!--Main Footer-->
<footer class="main-footer">

	<div class="auto-container">

		<!--Widgets Section-->
		<div class="widgets-section">
			<div class="row clearfix">

				<!--big column-->
				<div class="big-column col-md-5 col-sm-12">
					<div class="row clearfix">

						<!--Footer Column-->
						<div class="footer-column col-sm-12">
							<div class="footer-widget about-widget">
								<div class="logo">
									<a href="index-2.html"><img src="images/footer-logo.png" style="width: 300px;" alt="" /></a>
								</div>
								<div class="text">Established in the year 2000 Airtech offers best engineering solution for a wide spectrum of customers in areas related to Heating, Ventilation & Air Conditioning (HVAC), and is the largest and the most preferred air conditioning company in Nepal. We focus on excellence and personalized service to provide the highest quality professional installation of (HVAC) Heating, Ventilation and Air Conditioning systems.</div>
								<a href="#" class="theme-btn btn-style-four">About Company</a>
							</div>
						</div>

						<!--Footer Column-->

					</div>
				</div>

				<!--big column-->
				<div class="big-column col-md-5 col-sm-12">
					<div class="row clearfix justify-content-end">

						<!--Footer Column-->


						<!--Footer Column-->
						<div class="footer-column col-md-7 col-sm-12">
							<div class="footer-widget contact-widget">
								<h2>Contact</h2>
								<div class="number">+977-1-4219999,4101605</div>
								<ul>
									<li>1st Floor, Sharada Complex, Panchyan Marg, Thapathali, Kathmandu, Nepal</li>
									<li><a href="#">info@airtech.com.np</a></li>
								</ul>
							</div>
						</div>

					</div>
				</div>

			</div>
		</div>

		<!-- Footer Bottom -->
		<div class="footer-bottom">
			<div class="clearfix">
				<div class="pull-left">
					<div class="copyright">&copy; 2020 Airtech. All rights reserved.</div>
				</div>

			</div>
		</div>

	</div>
</footer>

</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-up"></span></div>
<!--Scroll to top-->

<script src="/js/jquery.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<!--Revolution Slider-->
<script src="/plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="/plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="/plugins/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="/plugins/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="/plugins/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script src="/plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="/plugins/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="/plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="/js/main-slider-script.js"></script>
<!--Revolution Slider-->

<script src="/js/jquery-ui.js"></script>
<script src="/js/validate.js"></script>
<script src="/js/jquery.fancybox.js"></script>
<script src="/js/owl.js"></script>
<script src="/js/wow.js"></script>
<script src="/js/appear.js"></script>

<script src="/js/script.js"></script>

<?php 
if(function_exists('get_scripts')){
	get_scripts();
}

?>
</body>


</html>