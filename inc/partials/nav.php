<ul class="navigation clearfix">
    <li><a href="/">Home</a>

    </li>
    <li><a href="/about.php">About us</a></li>
    <li class='dropdown'><a href="/services.php">Services</a>
        <ul>
            <?php foreach ($services as $service) : ?>

                <li><a href='/services/<?php echo $service->slug . ".php"; ?>'><?php echo $service->title ?> </a></li>
            <?php endforeach; ?>
        </ul>
    </li>
    <li class='dropdown'><a href="/projects.php">Projects</a>
        <ul>

            <?php foreach ($projects as $project) : ?>

                <li><a href='/projects/<?php echo $project->slug . ".php"; ?>'><?php echo $project->title ?> </a></li>
            <?php endforeach; ?>
        </ul>
    </li>
    <li><a href="/careers.php">Careers</a>

    </li>
    <li><a href="/contact_us.php">Contact Us</a>

    </li>

</ul>