<section class="clients-section">
	<div class="auto-container">
		<div class="logo-icon flaticon-settings-4"></div>
		<!-- Sec Title -->
		<div class="sec-title centered">
			<h2>Client’s Credentials</h2>
		</div>

		<div class="single-item-carousel owl-carousel owl-theme">

			<!-- Testimonial Block -->
			<div class="testimonial-block">
				<div class="inner-box">
					<div class="image-outer">
						<!-- Quote Left -->
						<!--<div class="quote quote-left">-->
						<!--	<span class="icon flaticon-right-quotation-sign"></span>-->
						<!--</div>-->
						<!-- Quote Right -->
						<!--<div class="quote quote-right">-->
						<!--	<span class="icon flaticon-right-quotation-sign"></span>-->
						<!--</div>-->
						<!--<div class="image">-->
						<!--	<img src="images/resource/author-1.jpg" alt="" />-->
						<!--</div>-->
					</div>
					<div class="text">"Airtech has successfully completed the HVAC works within agreed period and terms. They are deputed a competent and technically sound team to complete the work in accordance to our requirement."
					</div>
					<h3> Manish Shrestha</h3>
					<div class="location">Estate Manager - British Embassy, Kathmandu</div>
				</div>
			</div>

			<!-- Testimonial Block -->
			<div class="testimonial-block">
				<div class="inner-box">
					<div class="image-outer">
						<!-- Quote Left -->
						
						<!-- Quote Right -->
					
					<!--	<div class="image">-->
					<!--		<img src="images/resource/author-1.jpg" alt="" />-->
					<!--	</div>-->
					<!--</div>-->
					</div>
				
					<div class="text">
						
					"It was a great experience to work with Airtech for the entire MEP works at Fairfield Marroitt Project at Thamel, Kathmandu. They were totally committed to ensuring all fire and life safety clearances to the highest standards. We are also working with Airtech at our new Marriott - Naxal project and we look forward to work with them for our future projects as well."
	
					</div>
				
					<h3>Gaurav Agarwal</h3>
					<div class="location">Managing Director - Nepal Hospitality and Hotel (P) Ltd.
					</div>
				</div>
			</div>

			<!-- Testimonial Block -->
			<div class="testimonial-block">
				<div class="inner-box">
					<div class="image-outer">
						<!-- Quote Left -->
						<!--<div class="quote quote-left">-->
						<!--	<span class="icon flaticon-right-quotation-sign"></span>-->
						<!--</div>-->
						<!-- Quote Right -->
						<!--<div class="quote quote-right">-->
						<!--	<span class="icon flaticon-right-quotation-sign"></span>-->
						<!--</div>-->
						<!--<div class="image">-->
						<!--	<img src="images/resource/author-1.jpg" alt="" />-->
						<!--</div>-->
					</div>
					<div class="text">"We are very much satisfied with their work and highly recommend them for other projects of similar nature."</div>
					<h3>Suraj Man Mahanrjan</h3>
					<div class="location">Chief Engineer, Tiger One</div>
				</div>
			</div>
			
			<div class="testimonial-block">
				<div class="inner-box">
					<div class="image-outer">
						<!-- Quote Left -->
						<!--<div class="quote quote-left">-->
						<!--	<span class="icon flaticon-right-quotation-sign"></span>-->
						<!--</div>-->
						<!-- Quote Right -->
						<!--<div class="quote quote-right">-->
						<!--	<span class="icon flaticon-right-quotation-sign"></span>-->
						<!--</div>-->
						<!--<div class="image">-->
						<!--	<img src="images/resource/author-1.jpg" alt="" />-->
						<!--</div>-->
					</div>
					<div class="text">"We are very much satisfied with their work and highly recommend them for other projects of similar nature."</div>
					<h3>Prabin B. Panday</h3>
					<div class="location">Executive Director, Shangri-La Village Pokhara</div>
				</div>
			</div>
			
			<div class="testimonial-block">
				<div class="inner-box">
					<div class="image-outer">
						<!-- Quote Left -->
						<!--<div class="quote quote-left">-->
						<!--	<span class="icon flaticon-right-quotation-sign"></span>-->
						<!--</div>-->
						<!-- Quote Right -->
						<!--<div class="quote quote-right">-->
						<!--	<span class="icon flaticon-right-quotation-sign"></span>-->
						<!--</div>-->
						<!--<div class="image">-->
						<!--	<img src="images/resource/author-1.jpg" alt="" />-->
						<!--</div>-->
					</div>
					<div class="text">
			"Their after sales services has been consistently good till date and we have been placing our regular new orders to Airtech for our additional new requirements. We trust that Airtech is competent and capable to execute similar projects in HVAC System on turnkey basis as we are satisfied from their technical services as well as the performance of the equipments supplies and installed at our plan . We are very much hopeful to receive good services from Airtech in the future as well."

						</div>
					<h3>Mahendra B Amatya</h3>
					<div class="location">
Managing Director - Nepal Pharmaceuticals Pvt. Ltd.</div>
				</div>
			</div>
			
			<div class="testimonial-block">
				<div class="inner-box">
					<div class="image-outer">
						<!-- Quote Left -->
						<!--<div class="quote quote-left">-->
						<!--	<span class="icon flaticon-right-quotation-sign"></span>-->
						<!--</div>-->
						<!-- Quote Right -->
						<!--<div class="quote quote-right">-->
						<!--	<span class="icon flaticon-right-quotation-sign"></span>-->
						<!--</div>-->
						<!--<div class="image">-->
						<!--	<img src="images/resource/author-1.jpg" alt="" />-->
						<!--</div>-->
					</div>
					<div class="text">

"We are impressed by the technical knowledge and expertise of the Airtech team and would prefer them to be our project partner in our expansion project as well."

						</div>
					<h3>Ravi Chaudhary</h3>
					<div class="location">
General Manager - Norvic International Hospital and Medical College</div>
				</div>
			</div>
			
			
			<div class="testimonial-block">
				<div class="inner-box">
					<div class="image-outer">
						<!-- Quote Left -->
						<!--<div class="quote quote-left">-->
						<!--	<span class="icon flaticon-right-quotation-sign"></span>-->
						<!--</div>-->
						<!-- Quote Right -->
						<!--<div class="quote quote-right">-->
						<!--	<span class="icon flaticon-right-quotation-sign"></span>-->
						<!--</div>-->
						<!--<div class="image">-->
						<!--	<img src="images/resource/author-1.jpg" alt="" />-->
						<!--</div>-->
					</div>
					<div class="text">

"We are pleased to inform that Airtech has sucessfully completed supply, delivery, installation, testing and commissioning MEP works at our Hospital within stipulated time period. We are very much satisfied with their professional works and highly recommend them for other similar projects."

						</div>
					<h3>Naveen Bhatt</h3>
					<div class="location">
GM - Project. - Nepal Mediciti - Ashwins Medical College and Hospital Pvt. Ltd.</div>
				</div>
			</div>
			
			




		</div>

	</div>
</section>

<style>
	.testimonial-block div.image-outer{
		height:0px !important;	
	}
</style>