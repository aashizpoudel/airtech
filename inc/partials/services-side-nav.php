<?php function side_nav($slug)
{
	global $services;

?>
	<ul class="services-categories">

		<?php foreach ($services as $service) : ?>

			<li <?php if ($slug == $service->slug) : ?> class='active' <?php endif; ?>><a href="/services/<?php echo $service->slug; ?>.php"><?php echo $service->title; ?></a></li>


		<?php endforeach; ?>
	</ul>



<?php

}

?>