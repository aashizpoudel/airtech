 <div class="sidebar-widget services-widget">
							<div class="widget-content" style="background-image:url(/images/resource/support.jpg);">
								<div class="icon flaticon-settings-4"></div>
								<h3>Optimising Perfomance with Special Services</h3>
								<div class="text">Discover how we're improving <br> quality of industries</div>
								<a href="/contact_us.php" class="theme-btn btn-style-two">get in touch</a>
							</div>
						</div>
						
						<!-- Support Widget -->
                        <div class="sidebar-widget support-widget">
                            <div class="widget-content">
                                <span class="icon flaticon-telephone-1"></span>
                                <div class="text">Got any Questions? <br> Call us Today!</div>
                                <div class="number">01-4219999</div>
                                <div class="email"><a href="#">info@airtech.com.np</a></div>
                            </div>
                        </div>