<?php
if (!isset($images) || empty($images)) {
    $images = [
        'https://picsum.photos/200/300',
        'https://picsum.photos/200/300',
        'https://picsum.photos/200/300',
        'https://picsum.photos/200/300',
        'https://picsum.photos/200/300'

    ];
}

?>
<div style='margin-bottom:16px' class="slider">
    <?php
    foreach ($images as $slide) :
    ?>
        <img style="width:100%" src="<?php echo $slide; ?>" alt="" srcset="">

    <?php endforeach; ?>
</div>

<?php
function get_scripts()
{
?>
    <script src="/js/sss.min.js"></script>
    <link rel="stylesheet" href="/css/sss.css" type="text/css" media="all">
    <script>
        jQuery(function($) {
            $('.slider').sss();
        });
    </script>
<?php
}
?>