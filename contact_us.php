<?php 
$page_title='Contact Us';


include('inc/header.php');
?>

<?php 

if(isset($_POST['submit-form'])){
	$name = $_POST['username']." ".$_POST['lastname'];
	$to = "admin@rabimsoft.com";
	$from=$_POST['email'];
	$subject = $_POST['subject'];
	$message= $_POST['message']."\n\n Phone: ".$_POST['phone'];
	$headers = "From: noreply@rabimsoft.com" . "\r\n" .
    "Reply-To: $from" . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

mail($to, $subject, $message, $headers);
$success = true;
}	

?>


 <section class="page-banner" style="background-image:url(images/background/3.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>Contact us</h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
                    <li>Contact us</li>
                </ul>
            </div>
        </div>
    </section>
    
<section class="contact-page-section">
		<div class="auto-container">
			<div class="row clearfix">
				
				<!-- Info Column -->
				<div class="info-column col-lg-4 col-md-12 col-sm-12">
					<div class="inner-column wow fadeInLeft animated" data-wow-delay="0ms" style="visibility: visible; animation-delay: 0ms; animation-name: fadeInLeft;">
						<!-- Title Box -->
						<div class="title-box">
							<h3>Contact Details</h3>
							<div class="title-text">Get in touch with us for any questions about our industries or projects.</div>
						</div>
						<ul class="contact-info-list">
							<li><span class="icon icon-home"></span><strong>Head Office</strong>
1st Floor, Sharada Complex, Panchyan Marg, Thapathali, Kathmandu, Nepal

							</li>
							<li><span class="icon icon-envelope-open"></span><strong>Email us</strong>info@airtech.com.np</li>
							<li><span class="icon icon-call-in"></span><strong>Phone</strong>+977-1-4219999</li>
						</ul>
						
						<!-- Social Links -->
				
						
					</div>
				</div>
				
				<!-- Form Column -->
				<div class="form-column col-lg-8 col-md-12 col-sm-12">
					<div class="inner-column wow fadeInRight animated" data-wow-delay="0ms" style="visibility: visible; animation-delay: 0ms; animation-name: fadeInRight;">
						<!-- Sec Title -->
						<div class="sec-title">
							<div class="title">Airtech Nepal</div>
							<h2>Send a Message</h2>
						</div>
						
						<!-- Contact Form -->
						<div class="contact-form">
							<?php if(isset($success)): ?>
							
							<p class='alert-primary'>Thank you for contacting us. We will get back to you soon.</p>

							<?php endif; ?>
							<form method="post" action="contact_us.php" id="contact-form" novalidate="novalidate">
								<div class="row clearfix">
									
									<div class="col-lg-6 col-md-6 col-sm-12 form-group">
										<input type="text" name="username" placeholder="First Name " required="">
									</div>
									
									<div class="col-lg-6 col-md-6 col-sm-12 form-group">
										<input type="text" name="lastname" placeholder="Last Name " required="">
									</div>
									
									<div class="col-lg-6 col-md-6 col-sm-12 form-group">
										<input type="email" name="email" placeholder="Email " required="">
									</div>
									
									<div class="col-lg-6 col-md-6 col-sm-12 form-group">
										<input type="text" name="phone" placeholder="Phone ">
									</div>
									
									<div class="col-lg-6 col-md-6 col-sm-12 form-group">
										<input type="text" name="subject" placeholder="Subject " required="">
									</div>
									
									<div class="col-lg-12 col-md-12 col-sm-12 form-group">
										<textarea name="message" placeholder="Message "></textarea>
									</div>
								
									<div class="col-lg-12 col-md-12 col-sm-12 form-group">
										<button class="theme-btn btn-style-five" type="submit" name="submit-form">Send Now</button>
									</div>
									
								</div>
							</form>
							
								
						</div>
						
					</div>
				</div>
				
			</div>
		</div>
	</section>
	
	 <section class="contact-map-section">
        <div class="outer-container">
            <div class="map-outer">
               <div>
               	<div style="overflow:hidden;width: 100%;position: relative;"><iframe width="100%" height='399px' src="https://maps.google.com/maps?height=499&amp;hl=en&amp;q=Thapathali+(Title)&amp;ie=UTF8&amp;t=&amp;z=13&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><div style="position: absolute;width: 80%;bottom: 10px;left: 0;right: 0;margin-left: auto;margin-right: auto;color: #000;text-align: center;"></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div><br />
               </div>
            </div>
        </div>
    </section>




<?php include('inc/footer.php');?>