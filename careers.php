<?php 
$page_title='Careers';
include('inc/header.php');
?>

<?php 

if(isset($_POST['submit-form'])){
	$name = $_POST['username']." ".$_POST['lastname'];
	$to = "admin@rabimsoft.com";
	$from=$_POST['email'];
	$subject = "Job Application";
	$message= $_POST['message']."\n\n Phone: ".$_POST['phone'];
	$headers = "From: noreply@rabimsoft.com" . "\r\n" .
    "Reply-To: $from" . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

mail($to, $subject, $message, $headers);
$success = true;
}	

?>


 <section class="page-banner" style="background-image:url(images/background/3.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>Careers</h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
                    <li>Careers</li>
                </ul>
            </div>
        </div>
    </section>
    
<section class="contact-page-section">
		<div class="auto-container">
			<div class="row clearfix">
				
				<!-- Info Column -->
			
				
				<!-- Form Column -->
				<div class="form-column col-lg-12 col-md-12 col-sm-12">
					<div class="inner-column wow fadeInRight animated" data-wow-delay="0ms" style="visibility: visible; animation-delay: 0ms; animation-name: fadeInRight;">
						<!-- Sec Title -->
				
						
						<!-- Contact Form -->
				<div class='row'>
						<div class='col-md-5 mt-2'>
						<h3 class='mb-4'>Vacancies</h3>
						<div class='text' style='width:80%'>
							There are no vacancies currently.
							<br>
							<br>
							Please send us your CV and we will notify you if any opportunities arise in the future.
						</div>
					</div>
					
					<div class='col-md-7 mt-2'>
						<h3 class='mb-4'>Send us your CV</h3>
							<div class="contact-form">
							<?php if(isset($success)): ?>
							
							<p class='alert-primary'>Thank you for contacting us. We will get back to you soon.</p>

							<?php endif; ?>
							<form method="post" action="careers.php" id="contact-form" novalidate="novalidate">
								<div class="row clearfix">
									
									<div class="col-lg-6 col-md-6 col-sm-12 form-group">
										<input type="text" name="username" placeholder="First Name " required="">
									</div>
									
									<div class="col-lg-6 col-md-6 col-sm-12 form-group">
										<input type="text" name="lastname" placeholder="Last Name " required="">
									</div>
									
									<div class="col-lg-6 col-md-6 col-sm-12 form-group">
										<input type="email" name="email" placeholder="Email " required="">
									</div>
									
									<div class="col-lg-6 col-md-6 col-sm-12 form-group">
										<input type="text" name="phone" placeholder="Phone ">
									</div>
									
									
									<div class="col-lg-12 col-md-12 col-sm-12 form-group">
										<textarea name="message" placeholder="Resume "></textarea>
									</div>
								
									<div class="col-lg-12 col-md-12 col-sm-12 form-group">
										<button class="theme-btn btn-style-five" type="submit" name="submit-form">Send Now</button>
									</div>
									
								</div>
							</form>
							
								
						</div>
					</div>
				</div>
						
					</div>
				</div>
				
			</div>
		</div>
	</section>
	
	<style>
	h3{
		    position: relative;
    color: #001e57;
    font-size: 24px;
    margin-bottom: 24px;
    padding-bottom: 22px;
    font-weight: 900;
	}
		h3:before{
			position: absolute;
    content: '';
    left: 0px;
    bottom: 0px;
    height: 1px;
    width: 50px;
    background-color: #df6512;
		}
	</style>



<?php include('inc/footer.php');?>